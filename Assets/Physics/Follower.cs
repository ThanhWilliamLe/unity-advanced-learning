﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follower : MonoBehaviour
{
    public GameObject follow;
    public bool rotate;
    public bool skipX;
    public bool skipY;
    public bool skipZ;

    public float delay;

    Vector3 diffRot;
    Vector3 diffPos;
    Vector3 diffPos0;
    Quaternion oriRot;

    Quaternion prevTargetRot;
    Vector3 prevTargetPos;

    Coroutine runningFollow;

    void Start()
    {
        Init();
    }

    [ContextMenu("Reset")]
    void Init()
    {
        diffRot = transform.rotation.eulerAngles - follow.transform.rotation.eulerAngles;
        diffPos = transform.position - follow.transform.position;
        diffPos0 = Quaternion.Inverse(transform.rotation) * diffPos;
        oriRot = transform.rotation;
    }

    void Update()
    {
        if (prevTargetPos != follow.transform.position || prevTargetRot != follow.transform.rotation)
        {
            //if (runningFollow != null) StopCoroutine(runningFollow);
            runningFollow = StartCoroutine(FollowDelay());
        }
        prevTargetRot = follow.transform.rotation;
        prevTargetPos = follow.transform.position;
    }

    IEnumerator FollowDelay()
    {
        Quaternion initRot = transform.rotation;
        Vector3 initPos = transform.position;

        Quaternion targetRot = transform.rotation;
        Vector3 targetPos = transform.position;
        if (rotate)
        {
            targetRot = Quaternion.Euler(follow.transform.rotation.eulerAngles + diffRot);
            targetPos = follow.transform.position + transform.rotation * diffPos0;
        }
        else
        {
            targetRot = oriRot;
            Vector3 newPos = follow.transform.position + diffPos;
            if (skipX) newPos.x = transform.position.x;
            if (skipY) newPos.y = transform.position.y;
            if (skipZ) newPos.z = transform.position.z;
            targetPos = newPos;
        }

        float time = 0;
        while (time <= delay)
        {
            time += Time.deltaTime;
            float ratio = delay == 0 ? 1 : Mathf.Clamp01(time / delay);
            transform.rotation = Quaternion.Lerp(initRot, targetRot, ratio);
            transform.position = Vector3.Lerp(initPos, targetPos, ratio);

            if (time >= delay) yield break;
            yield return null;
        }
    }
}
