﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrajectorySim : MonoBehaviour
{
    public Rigidbody target;
    public Transform drawCenter;

    public LineRenderer lr;
    public float timeSimulate = 1f;
    public float details = 0.15f;

    public void SimulateForce(Vector3 impulse, ForceMode fm)
    {
        Vector3 currentVelo = impulse;
        switch (fm)
        {
            case ForceMode.Impulse:
                currentVelo = currentVelo / target.mass + target.velocity;
                break;
            case ForceMode.Force:
                currentVelo = currentVelo * Time.deltaTime / target.mass + target.velocity;
                break;
            case ForceMode.Acceleration:
                currentVelo = currentVelo * Time.deltaTime + target.velocity;
                break;
            case ForceMode.VelocityChange:
                currentVelo = impulse;
                break;
        }

        Vector3 pos = drawCenter.position;
        Vector3 prevPos = pos;
        float mass = target.mass;
        float through = 0;

        List<Vector3> poss = new List<Vector3> { pos };
        while (through <= timeSimulate)
        {
            bool hitSomething = Physics.Raycast(prevPos, currentVelo, (currentVelo).magnitude, Physics.DefaultRaycastLayers);

            through += details;
            if (target.useGravity) currentVelo += Physics.gravity * details;
            currentVelo *= (1 - target.drag * details);
            pos += currentVelo * details;
            poss.Add(pos);

            if (hitSomething) break;
        }

        lr.positionCount = poss.Count;
        lr.SetPositions(poss.ToArray());
    }

    public void Reset()
    {
        lr.positionCount = 0;
    }
}
