﻿using UnityEngine;

public class InputControl : MonoBehaviour
{
    public Camera cam;
    public LineRenderer lr;
    public Rigidbody rb;
    public float power;
    public TrajectorySim sim;

    [Space]
    public KeyCode xAdd = KeyCode.D;
    public KeyCode xSub = KeyCode.A;
    public KeyCode yAdd = KeyCode.R;
    public KeyCode ySub = KeyCode.F;
    public KeyCode zAdd = KeyCode.W;
    public KeyCode zSub = KeyCode.S;
    public KeyCode longer = KeyCode.E;
    public KeyCode shorter = KeyCode.Q;
    public KeyCode stop = KeyCode.X;
    public KeyCode center = KeyCode.LeftShift;
    public KeyCode shoot = KeyCode.Space;
    public KeyCode reset = KeyCode.Backspace;

    [Space]
    public Transform centerAt;
    public Vector3 shootCenter = Vector3.zero;
    public Vector3 shootDir = Vector3.zero;

    void Update()
    {
        Vector3 add = Vector3.zero;
        if (Input.GetKey(xAdd)) add.x += 1;
        if (Input.GetKey(xSub)) add.x -= 1;
        if (Input.GetKey(yAdd)) add.y += 1;
        if (Input.GetKey(ySub)) add.y -= 1;
        if (Input.GetKey(zAdd)) add.z += 1;
        if (Input.GetKey(zSub)) add.z -= 1;
        add = cam.transform.rotation * add * Time.deltaTime;

        if (Input.GetKey(center)) shootCenter += add;
        else shootDir += add;

        if (Input.GetKey(longer)) shootDir *= 1f + 1 * Time.deltaTime;
        if (Input.GetKey(shorter)) shootDir /= 1f + 1 * Time.deltaTime;

        if (lr.positionCount != 2) lr.positionCount = 2;
        lr.SetPosition(0, centerAt.position + shootCenter);
        lr.SetPosition(1, centerAt.position + shootCenter + shootDir);

        if (shootDir.sqrMagnitude > 0) sim.SimulateForce(-shootDir * power, ForceMode.Impulse);
        else sim.Reset();

        if (Input.GetKey(shoot)) Shoot();
        if (Input.GetKey(reset)) Reset(true);
        if (Input.GetKey(stop)) StopMoving();
    }

    public void Shoot()
    {
        if (shootDir.sqrMagnitude > 0)
        {
            rb.AddForceAtPosition(-shootDir * power, rb.gameObject.transform.TransformPoint(shootCenter), ForceMode.Impulse);
        }
        Reset(true);
    }

    public void Reset(bool resetSim)
    {
        shootCenter = shootDir = Vector3.zero;
        if (resetSim) sim.Reset();
    }

    public void StopMoving()
    {
        rb.angularVelocity = rb.velocity = Vector3.zero;
    }
}
