﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "William/Unlit Gradient"
{
 Properties
 {
        [Toggle]_LocalScreen ("Local screen", Float) = 1
        _ColorTop ("Color top", Color) = (1,1,1,1)
        _ColorBottom ("Color bottom", Color) = (1,1,1,1)
 }
 SubShader
 {
     Tags { "RenderType"="Opaque" }
     LOD 100

     Pass
     {
         CGPROGRAM
         #pragma vertex vert
         #pragma fragment frag
         
         #include "UnityCG.cginc"

         struct appdata
         {
             float4 vertex : POSITION;
             float2 uv : TEXCOORD0;
         };

         struct v2f
         {
             float4 vertex : SV_POSITION;
             float2 uv : TEXCOORD0;
         };

         half _LocalScreen;
         float4 _ColorTop;
         float4 _ColorBottom;

         v2f vert (appdata v)
         {
             v2f o;
             o.vertex = UnityObjectToClipPos(v.vertex);

             if(_LocalScreen==1)
             {
                float4 p = UnityObjectToClipPos(v.vertex);
                o.uv = 1-(p.xy/p.w + 1) * 0.5;
             }
             else 
             {
                o.uv = v.uv;
             }
             return o;
         }
         
         fixed4 frag (v2f i) : SV_Target
         {
            fixed4 col = lerp(_ColorBottom, _ColorTop, i.uv.y);
            return col;
         }
         ENDCG
     }
 }
}
