﻿// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

Shader "William/Fogged Diffuse" 
{
 Properties 
 {
    _MainTex ("Albedo (RGB)", 2D) = "white" {}
    _Color ("Color", Color) = (1,1,1,1)

    _FogColor ("Fog color", Color) = (1,1,1,1)
    _FogStart ("Fog start", Float) = 20
    _FogEnd ("Fog end", Float) = 30
    //_FogLength ("Fog length", Float) = 30
    [Toggle]_LocalFog ("Local fog", Float) = 1
    [Toggle]_BottomFog ("Bottom fog only", Float) = 0
 }
 SubShader 
 {
     Tags{"RenderType" = "Opaque"}

     LOD 200

     CGPROGRAM
     #pragma surface surfaceFunc Lambert fullforwardshadows halfasview finalcolor:finalColor
     #pragma target 3.0

     sampler2D _MainTex;

     struct Input {
         float2 uv_MainTex;
         float3 worldPos;
     };

     fixed4 _Color;

     fixed4 _FogColor;
     float _FogStart;
     float _FogEnd;
     //float _FogLength;

     float _LocalFog;
     float _BottomFog;
     

     UNITY_INSTANCING_BUFFER_START(Props)
     UNITY_INSTANCING_BUFFER_END(Props)

     void surfaceFunc (Input IN, inout SurfaceOutput o) 
     {
         fixed4 c = tex2D (_MainTex, IN.uv_MainTex);
         c = lerp(c, c * _Color, _Color.a);
         o.Albedo = c.rgb;
     }

     void finalColor(Input IN, SurfaceOutput o, inout fixed4 color)
     {
         float dist = 0;
         if(_LocalFog)
         {
            half3 localPos = mul(unity_WorldToObject,IN.worldPos);

            if(_BottomFog) dist = -localPos.y;
            else dist = length(float3(localPos.x, localPos.y+0.5, localPos.z));
         }
         else 
         {
            if(_BottomFog) dist = _WorldSpaceCameraPos.y - IN.worldPos.y;
            else distance(_WorldSpaceCameraPos, IN.worldPos);
         }

         float ratio = smoothstep(_FogStart, _FogEnd, dist);
         color = lerp(color, _FogColor, _FogColor.a * ratio);
     }
     ENDCG
 }
 FallBack "Diffuse"
}
