﻿Shader "William/Simple Diffuse" 
{
 Properties 
 {
    _MainTex ("Albedo (RGB)", 2D) = "white" {}
    _Color ("Color", Color) = (1,1,1,1)
 }
 SubShader 
 {
     Tags{"RenderType" = "Opaque"}

     LOD 200

     CGPROGRAM
     #pragma surface surfaceFunc Lambert fullforwardshadows halfasview
     #pragma target 3.0

     sampler2D _MainTex;

     struct Input {
         float2 uv_MainTex;
     };

     fixed4 _Color;

     UNITY_INSTANCING_BUFFER_START(Props)
     UNITY_INSTANCING_BUFFER_END(Props)

     void surfaceFunc (Input IN, inout SurfaceOutput o) 
     {
         fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
         o.Albedo = c.rgb;
     }
     ENDCG
 }
 FallBack "Diffuse"
}
