﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "William/My First Shader" 
{ 

    Properties
    {
        _Tint ("Tint", Color) = (1,1,1,1)
        _MainTex ("Texture", 2D) = "gray"
    }


    SubShader { 
    Pass {

        CGPROGRAM
        #pragma vertex MyVertexProgram
        #pragma fragment MyFragmentProgram
        #include "UnityCG.cginc"

        float4 _Tint;
        sampler2D _MainTex;
        float4 _MainTex_ST;

        struct VertexData
        {
            float4 pos : POSITION;
            float3 uv : TEXCOORD0;
        };

        struct Interpolators 
        {
            float4 pos : SV_POSITION;
            float2 uv : TEXCOORD0;
            //float3 localPos : TEXCOORD0;
        };

        Interpolators MyVertexProgram(VertexData v)
        {
            Interpolators i;
            //i.localPos = v.pos.xyz;
            i.pos = UnityObjectToClipPos(v.pos);
            i.uv = TRANSFORM_TEX(v.uv, _MainTex);
            return i;
        }

        float4 MyFragmentProgram(Interpolators i) : SV_TARGET
        {
            return _Tint * tex2D(_MainTex, i.uv);
        }

        ENDCG

    }}

}