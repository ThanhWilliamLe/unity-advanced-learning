﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "William/My First Lighting Shader" 
{ 

    Properties
    {
        _Tint ("Tint", Color) = (1,1,1,1)
        _Metallic ("Metallic", Range(0, 1)) = 0
        _MainTex ("Albedo", 2D) = "white" {}
        _Smoothness ("Smoothness", Range(0, 1)) = 0.5
        [NoScaleOffset] _NormalMap ("Normals", 2D) = "bump" {}
    }


    SubShader 
    { 
        Pass 
        {
            Tags 
            {
                "LightMode" = "ForwardBase"
            }
            CGPROGRAM
            #pragma target 3.0
            #pragma multi_compile _ SHADOWS_SCREEN
            #pragma multi_compile _ VERTEXLIGHT_ON
            #pragma vertex MyVertexProgram
            #pragma fragment MyFragmentProgram
            #include "MyLighting.cginc"
            ENDCG
        }
        Pass 
        {
            Tags 
            {
                "LightMode" = "ForwardAdd"
            }
            Blend One One
            ZWrite Off
            CGPROGRAM
            #pragma target 3.0
            #pragma multi_compile_fwdadd
            #pragma vertex MyVertexProgram
            #pragma fragment MyFragmentProgram
            #include "MyLighting.cginc"
            ENDCG
        }
        Pass 
        {
            Tags 
            {
                "LightMode" = "ShadowCaster"
            }

            CGPROGRAM

            #pragma target 3.0

            #pragma vertex MyShadowVertexProgram
            #pragma fragment MyShadowFragmentProgram

            #include "MyShadows.cginc"

            ENDCG
        }
    }

}