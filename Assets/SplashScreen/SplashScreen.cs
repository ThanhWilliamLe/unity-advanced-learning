﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Video;

public class SplashScreen : MonoBehaviour
{
    public VideoPlayer videoPlayer;
    [Range(0,1)]
    public float minLoadRatio = 0.6f;
    public string sceneToLoad = "GameScene";
    public RectTransform progressBar;

    public void Start()
    {
        StartCoroutine(LoadScene());
    }

    IEnumerator LoadScene()
    {
        AsyncOperation a = SceneManager.LoadSceneAsync(sceneToLoad);
        a.allowSceneActivation = false;

        yield return new WaitUntil(() =>
        {
            float videoLoadProgress = Mathf.Clamp01((float)(videoPlayer.time / videoPlayer.clip.length / minLoadRatio));
            float sceneLoadProgress = Mathf.Clamp01(a.progress / 0.9f);
            float progress = Mathf.Min(videoLoadProgress, sceneLoadProgress);
            progressBar.anchorMax = new Vector2(progress, progressBar.anchorMax.y);

            return videoLoadProgress >= 1;
        });
        a.allowSceneActivation = true;
        yield return a;
    }
}
