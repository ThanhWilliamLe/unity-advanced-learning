﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using UnityEngine;

public class PerfCompareArray : MonoBehaviour
{

    void Start()
    {
        Random rand = new Random();
        Stopwatch sw = new Stopwatch();
        int tries = 10000;

        List<int> list = Enumerable.Range(0, 1000).ToList();

        sw.Reset();
        Log("{0,40}", "Accessing with for...");
        sw.Start();
        for (int i = 0; i < tries; i++)
        {
            int dummy = 0;
            for (int i1 = 0; i1 < list.Count; i1++) dummy = list[i1];
        }
        sw.Stop();
        Log("  Time used: {0,9} ticks", sw.ElapsedTicks);

        sw.Reset();
        Log("{0,40}", "Accessing with foreach...");
        sw.Start();
        for (int i = 0; i < tries; i++)
        {
            int dummy = 0;
            foreach (int i1 in list) dummy = list[i1];
        }
        sw.Stop();
        Log("  Time used: {0,9} ticks", sw.ElapsedTicks);
    }

    void Log(string s, params object[] p)
    {
        UnityEngine.Debug.LogFormat(s, p);
    }

}
