﻿using System.Collections.Generic;
using UnityEngine;

public static class GenMeshBox
{
    public static Mesh Generate(GenMesh gm)
    {
        int vertsCount =
            (gm.cubeDivX + gm.cubeDivZ + 2) * 2 * (gm.cubeDivY + 1) // side faces
            +
            (gm.cubeDivX + 1) * (gm.cubeDivZ + 1) * 2; //top bottom faces
        int trisCount = 12 * (gm.cubeDivX * gm.cubeDivY + gm.cubeDivX * gm.cubeDivZ + gm.cubeDivY * gm.cubeDivZ);
        Vector3[] verts = new Vector3[vertsCount];
        Vector2[] uvs = new Vector2[vertsCount];
        Vector3[] norms = new Vector3[vertsCount];
        int[] tris = new int[trisCount];

        Vector3 step = new Vector3(gm.size.x / gm.cubeDivX, gm.size.y / gm.cubeDivY, gm.size.z / gm.cubeDivZ);
        int i = 0; // count quad looped through
        int j = 0; // count vertex looped through

        // near right far left verts
        Vector3 start = -gm.size / 2f;
        Vector3 norm = new Vector3(0, 0, -1);
        for (int x = 0; x < gm.cubeDivX + 1; x++)
            for (int y = 0; y < gm.cubeDivY + 1; y++)
            {
                if (y != gm.cubeDivY && x != gm.cubeDivX) SetQuadTris(tris, i++, j, gm.cubeDivY);
                uvs[j] = new Vector2((float)x / gm.cubeDivX, (float)y / gm.cubeDivY);
                norms[j] = norm;
                verts[j] = start + new Vector3(step.x * x, step.y * y, 0);
                j++;
            }
        start.x = gm.size.x / 2f;
        norm = new Vector3(1, 0, 0);
        for (int z = 0; z < gm.cubeDivZ + 1; z++)
            for (int y = 0; y < gm.cubeDivY + 1; y++)
            {
                if (y != gm.cubeDivY && z != gm.cubeDivZ) SetQuadTris(tris, i++, j, gm.cubeDivY);
                uvs[j] = new Vector2((float)z / gm.cubeDivZ, (float)y / gm.cubeDivY);
                norms[j] = norm;
                verts[j] = start + new Vector3(0, step.y * y, step.z * z);
                j++;
            }
        start.z = gm.size.z / 2f;
        norm = new Vector3(0, 0, 1);
        for (int x = 0; x < gm.cubeDivX + 1; x++)
            for (int y = 0; y < gm.cubeDivY + 1; y++)
            {
                if (y != gm.cubeDivY && x != gm.cubeDivX) SetQuadTris(tris, i++, j, gm.cubeDivY);
                uvs[j] = new Vector2((float)x / gm.cubeDivX, (float)y / gm.cubeDivY);
                norms[j] = norm;
                verts[j] = start + new Vector3(-step.x * x, step.y * y, 0);
                j++;
            }
        start.x = -gm.size.x / 2f;
        norm = new Vector3(-1, 0, 0);
        for (int z = 0; z < gm.cubeDivZ + 1; z++)
            for (int y = 0; y < gm.cubeDivY + 1; y++)
            {
                if (y != gm.cubeDivY && z != gm.cubeDivZ) SetQuadTris(tris, i++, j, gm.cubeDivY);
                uvs[j] = new Vector2((float)z / gm.cubeDivZ, (float)y / gm.cubeDivY);
                norms[j] = norm;
                verts[j] = start + new Vector3(0, step.y * y, -step.z * z);
                j++;
            }

        // top bottom inner verts
        start.y = gm.size.y / 2f;
        norm = new Vector3(0, 1, 0);
        for (int z = 0; z < gm.cubeDivZ + 1; z++)
            for (int x = 0; x < gm.cubeDivX + 1; x++)
            {
                if (x != gm.cubeDivX && z != gm.cubeDivZ) SetQuadTris(tris, i++, j, gm.cubeDivX);
                uvs[j] = new Vector2((float)x / gm.cubeDivX, 1 - (float)z / gm.cubeDivZ);
                norms[j] = norm;
                verts[j] = start + new Vector3(step.x * x, 0, -step.z * z);
                j++;
            }
        start.y = -gm.size.y / 2f;
        norm = new Vector3(0, -1, 0);
        for (int x = 0; x < gm.cubeDivX + 1; x++)
            for (int z = 0; z < gm.cubeDivZ + 1; z++)
            {
                if (x != gm.cubeDivX && z != gm.cubeDivZ) SetQuadTris(tris, i++, j, gm.cubeDivZ);
                uvs[j] = new Vector2((float)x / gm.cubeDivX, (float)z / gm.cubeDivZ);
                norms[j] = norm;
                verts[j] = start + new Vector3(step.x * x, 0, -step.z * z);
                j++;
            }

        Mesh m = new Mesh
        {
            name = "WG-Cube",
            vertices = verts,
            uv = uvs,
            uv2 = uvs,
            uv3 = uvs,
            uv4 = uvs,
            normals = norms,
            triangles = tris
        };

        m.RecalculateBounds();
        //m.RecalculateNormals();
        m.RecalculateTangents();
        return m;
    }

    public static void SetQuadTris(int[] tris, int i, int j, int n)
    {
        tris[i * 6 + 0] = j;
        tris[i * 6 + 1] = j + 1;
        tris[i * 6 + 2] = j + n + 2;
        tris[i * 6 + 3] = j;
        tris[i * 6 + 4] = j + n + 2;
        tris[i * 6 + 5] = j + n + 1;
        //if (backToBeg)
        //{
        //    tris[i * 6 + 2] = j % (n + 1) + 1;
        //    tris[i * 6 + 4] = j % (n + 1) + 1;
        //    tris[i * 6 + 5] = j % (n + 1);
        //}
    }

    // not matching with mesh
    public static List<Collider> GenColliders(GenMesh gm)
    {
        float xSize = gm.size.x;
        float ySize = gm.size.y;
        float zSize = gm.size.z;
        float roundness = 0;
        Vector3 min = Vector3.one * roundness;
        Vector3 half = new Vector3(xSize, ySize, zSize) * 0.5f;
        Vector3 max = new Vector3(xSize, ySize, zSize) - min;

        List<Collider> colliders = new List<Collider>();

        colliders.Add(BoxCollider(xSize, ySize - roundness * 2, zSize - roundness * 2));
        colliders.Add(BoxCollider(xSize - roundness * 2, ySize, zSize - roundness * 2));
        colliders.Add(BoxCollider(xSize - roundness * 2, ySize - roundness * 2, zSize));

        colliders.Add(CapsuleCollider(0, half.x, min.y, min.z, roundness));
        colliders.Add(CapsuleCollider(0, half.x, min.y, max.z, roundness));
        colliders.Add(CapsuleCollider(0, half.x, max.y, min.z, roundness));
        colliders.Add(CapsuleCollider(0, half.x, max.y, max.z, roundness));

        colliders.Add(CapsuleCollider(1, min.x, half.y, min.z, roundness));
        colliders.Add(CapsuleCollider(1, min.x, half.y, max.z, roundness));
        colliders.Add(CapsuleCollider(1, max.x, half.y, min.z, roundness));
        colliders.Add(CapsuleCollider(1, max.x, half.y, max.z, roundness));

        colliders.Add(CapsuleCollider(2, min.x, min.y, half.z, roundness));
        colliders.Add(CapsuleCollider(2, min.x, max.y, half.z, roundness));
        colliders.Add(CapsuleCollider(2, max.x, min.y, half.z, roundness));
        colliders.Add(CapsuleCollider(2, max.x, max.y, half.z, roundness));

        return colliders;
    }

    static BoxCollider BoxCollider(float x, float y, float z)
    {
        BoxCollider bc = new BoxCollider();
        bc.size = new Vector3(x, y, z);
        return bc;
    }

    static CapsuleCollider CapsuleCollider(int direction, float x, float y, float z, float roundness)
    {
        CapsuleCollider c = new CapsuleCollider();
        c.center = new Vector3(x, y, z);
        c.direction = direction;
        c.radius = roundness;
        c.height = c.center[direction] * 2f;
        return c;
    }
}