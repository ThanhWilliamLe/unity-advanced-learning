﻿using System;

public static class Helpers
{
#if UNITY_EDITOR
    public static bool isEditor = true;
#else
    public static bool isEditor = false;
#endif

}