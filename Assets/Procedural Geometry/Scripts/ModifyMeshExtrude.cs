﻿using System;
using System.Collections.Generic;
using UnityEngine;

public static class ModifyMeshExtrude
{
    public static Mesh ExtrudePolygons(ModifyMesh mm, Mesh m, Func<int, bool> randomFormula)
    {
        if (mm.extrudeRatio <= 0) return m;

        MeshData md = new MeshData(m);
        int triis = md.tris.Count;
        bool[] extrudeFlags = new bool[triis];
        for (int i = 0; i < triis; i += 3)
        {
            if (!randomFormula(i)) continue;
            extrudeFlags[md.tris[i]] = true;
            extrudeFlags[md.tris[i + 1]] = true;
            extrudeFlags[md.tris[i + 2]] = true;
        }
        int[] extrudeIds = md.VertBoolsToIds(extrudeFlags);

        List<Polygon> extrudePolys = md.GetPolys(extrudeIds);
        foreach (Polygon poly in extrudePolys)
        {
            ExtrudePoly(md, poly, mm.extrudeLength, mm.extrudeUnidir);
        }

        md.RemoveAllStandaloneVerts();
        md.MergeSimilars();
        return md.ToMesh(m.name);
    }

    public static Mesh ExtrudeTriangles(ModifyMesh mm, Mesh m, int grouping)
    {
        grouping = Mathf.Max(1, grouping);
        int skips = Mathf.Max(0, mm.extrudeSkips);
        MeshData md = new MeshData(m);
        int triMax = md.tris.Count;
        for (int i = 0; i < triMax; i += 3 * (skips + 1) * grouping)
        {
            Polygon poly = null;
            for (int j = 0; j < grouping; j++)
            {
                int iReal = i + j * 3;
                Triangle t = new Triangle(md.tris[iReal], md.tris[iReal + 1], md.tris[iReal + 2], iReal);
                //ExtrudeTri(md, t, mm.extrudeLength, mm.extrudeDirAvg);
                if (poly == null) poly = new Polygon(t);
                else poly.AddAdjacentTriangle(t);
            }
            ExtrudePoly(md, poly, mm.extrudeLength, mm.extrudeUnidir);
        }
        return md.ToMesh(m.name);
    }

    public static void ExtrudePoly(MeshData md, Polygon poly, float length, bool avgExtrusion)
    {
        int[] polyVerts = poly.GetVerts();
        int[] polyInnerVerts = md.GetInnerVerts(polyVerts);
        List<Edge> polyBoundary = md.GetEdges(polyVerts);
        polyBoundary.ToBoundaries();
        polyBoundary.SortBoundaryEdges();
        int[] polyOuterVerts = polyBoundary.ToBoundaryInts();
        int[] polyDupedOuterVerts = new int[polyOuterVerts.Length];

        Vector3 avgExtrude = Vector3.zero;
        foreach (int vert in polyVerts)
        {
            avgExtrude += md.norms[vert];
        }
        avgExtrude = avgExtrude.normalized * length;

        foreach (int vert in polyInnerVerts)
        {
            md.verts[vert] += (avgExtrusion ? avgExtrude : md.norms[vert]) * length;
        }
        for (int i = 0; i < polyOuterVerts.Length; i++)
        {
            int unshiftedVert = polyOuterVerts[i];
            int shiftedVert = md.DupeAndShift(unshiftedVert, (avgExtrusion ? avgExtrude : md.norms[unshiftedVert]) * length);

            polyDupedOuterVerts[i] = shiftedVert;
            foreach (Triangle tri in poly.triangles)
            {
                if (tri.HasVert(unshiftedVert))
                {
                    if (md.tris[tri.triIndex] == unshiftedVert) md.tris[tri.triIndex] = shiftedVert;
                    if (md.tris[tri.triIndex + 1] == unshiftedVert) md.tris[tri.triIndex + 1] = shiftedVert;
                    if (md.tris[tri.triIndex + 2] == unshiftedVert) md.tris[tri.triIndex + 2] = shiftedVert;
                }
            }
        }
        if (polyOuterVerts.Length > 0)
        {
            //List<int> stich1 = new List<int>(polyOuterVerts);
            //List<int> stich2 = new List<int>(polyDupedOuterVerts);
            //stich1.Add(polyOuterVerts[0]);
            //stich2.Add(polyDupedOuterVerts[0]);
            //md.Stich(stich1.ToArray(), stich2.ToArray());
            for (int i = 0; i < polyOuterVerts.Length; i++)
            {
                int i1 = (i + 1) % polyOuterVerts.Length;
                md.Stich(new int[] { polyOuterVerts[i], polyOuterVerts[i1] },
                         new int[] { polyDupedOuterVerts[i], polyDupedOuterVerts[i1] });
            }
        }
    }

    public static void ExtrudeTri(MeshData md, Triangle tri, float length, bool averageExtrusion)
    {
        Vector3 n1 = md.norms[tri.i1];
        Vector3 n2 = md.norms[tri.i2];
        Vector3 n3 = md.norms[tri.i3];
        Vector3 nAvg = (n1 + n2 + n3) / 3;
        int ii1 = md.DupeAndShift(tri.i1, averageExtrusion ? nAvg * length : n1 * length);
        int ii2 = md.DupeAndShift(tri.i2, averageExtrusion ? nAvg * length : n2 * length);
        int ii3 = md.DupeAndShift(tri.i3, averageExtrusion ? nAvg * length : n3 * length);
        md.tris[tri.triIndex] = ii1;
        md.tris[tri.triIndex + 1] = ii2;
        md.tris[tri.triIndex + 2] = ii3;
        md.Stich(new int[] { tri.i1, tri.i2 }, new int[] { ii1, ii2 });
        md.Stich(new int[] { tri.i2, tri.i3 }, new int[] { ii2, ii3 });
        md.Stich(new int[] { tri.i3, tri.i1 }, new int[] { ii3, ii1 });
    }
}
