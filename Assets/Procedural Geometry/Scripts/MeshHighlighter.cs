﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

[RequireComponent(typeof(MeshFilter))]
public class MeshHighlighter : MonoBehaviour
{

    public Mesh Mesh
    {
        get
        {
            if (meshFilter == null) meshFilter = gameObject.GetComponent<MeshFilter>();
            return Helpers.isEditor ? meshFilter.sharedMesh : meshFilter.mesh;
        }
    }

    public MeshFilter meshFilter;

    [Space]
    public List<int> highlightVerts = new List<int>();
    public List<Edge> highlightEdges = new List<Edge>();
    public List<Triangle> highlightTris = new List<Triangle>();
    public List<Polygon> highlightPolys = new List<Polygon>();

    [Space]
    public int backStep = 0;

    public void Clear()
    {
        highlightVerts.Clear();
        highlightEdges.Clear();
        highlightPolys.Clear();
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(MeshHighlighter))]
public class MeshHighlighterEditor : Editor
{
    static Color ColorVerts = new Color(0.15f, 0.2f, 1f, 1);
    static Color ColorEdges = new Color(0.2f, 1f, 0.2f, 1);
    static Color ColorTris = new Color(1f, 0.6f, 0.1f, 1);
    static Color ColorPolys = new Color(1f, 0.85f, 0.25f, 1);

    public override void OnInspectorGUI()
    {
        MeshHighlighter t = target as MeshHighlighter;
        if (t == null) return;

        base.OnInspectorGUI();
        if (GUILayout.Button("Clear"))
        {
            t.Clear();
        }
    }

    private void OnSceneGUI()
    {
        MeshHighlighter t = target as MeshHighlighter;
        if (t == null) return;

        Handles.matrix = t.gameObject.transform.localToWorldMatrix;
        Handles.zTest = UnityEngine.Rendering.CompareFunction.LessEqual;
        Mesh m = t.Mesh;
        int vc = m.vertexCount;

        if (t.highlightPolys != null && t.highlightPolys.Count > 0)
        {
            Handles.color = ColorPolys;
            int c = t.highlightPolys.Count;

            for (int i = 0; i < c; i++)
            {
                if (i >= c - t.backStep) break;

                Polygon poly = t.highlightPolys[i];
                Handles.color = ColorPolys + poly.colorAdd;
                foreach (Triangle triangle in poly.triangles)
                {
                    if (triangle.i1 >= vc) continue;
                    if (triangle.i2 >= vc) continue;
                    if (triangle.i3 >= vc) continue;
                    Handles.DrawAAConvexPolygon(new Vector3[] { m.vertices[triangle.i1], m.vertices[triangle.i2], m.vertices[triangle.i3] });
                }
            }
        }

        if (t.highlightTris != null && t.highlightTris.Count > 0)
        {
            Handles.color = ColorTris;
            int c = t.highlightTris.Count;

            for (int i = 0; i < c; i++)
            {
                if (i >= c - t.backStep) break;

                Triangle triangle = t.highlightTris[i];
                if (triangle.i1 >= vc) continue;
                if (triangle.i2 >= vc) continue;
                if (triangle.i3 >= vc) continue;
                Handles.DrawAAConvexPolygon(new Vector3[] { m.vertices[triangle.i1], m.vertices[triangle.i2], m.vertices[triangle.i3] });
            }
        }

        if (t.highlightEdges != null && t.highlightEdges.Count > 0)
        {
            Handles.color = ColorEdges;
            int c = t.highlightEdges.Count;
            for (int i = 0; i < c; i++)
            {
                if (i >= c - t.backStep) break;

                Edge e = t.highlightEdges[i];
                int i1 = e.i1;
                int i2 = e.i2;
                if (i1 < vc && i2 < vc) Handles.DrawDottedLine(m.vertices[i1], m.vertices[i2], 0.75f);
            }
        }

        if (t.highlightVerts != null && t.highlightVerts.Count > 0)
        {
            Handles.color = ColorVerts;
            int c = t.highlightVerts.Count;
            for (int i = 0; i < c; i++)
            {
                if (i >= c - t.backStep) break;

                int id = t.highlightVerts[i];
                if (id < vc) Handles.DrawWireDisc(m.vertices[id], m.normals[id], 0.1f);
            }
        }

    }
}
#endif