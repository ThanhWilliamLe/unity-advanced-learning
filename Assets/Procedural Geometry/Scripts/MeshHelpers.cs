﻿using System;
using System.Collections.Generic;
using UnityEngine;

public static class MeshHelpers
{
    public static void ToBoundaries(this List<Edge> l)
    {
        for (int i = l.Count - 1; i > 0; i--)
        {
            for (int j = i - 1; j >= 0; j--)
            {
                if (l[i].Equals(l[j]))
                {
                    l.RemoveAt(i);
                    l.RemoveAt(j);
                    i--;
                    break;
                }
            }
        }
    }

    public static List<Edge> SortBoundaryEdges(this List<Edge> edges)
    {
        for (int i = 0; i < edges.Count - 1; i++)
        {
            Edge e1 = edges[i];
            for (int i2 = i + 1; i2 < edges.Count; i2++)
            {
                Edge e2 = edges[i2];
                bool thisOne = false;
                if (e2.i1 == e1.i2) thisOne = true;
                else if (e2.i2 == e1.i2)
                {
                    thisOne = true;
                    e2.SwapVerts();
                }
                if (thisOne)
                {
                    edges.RemoveAt(i2);
                    edges.Insert(i + 1, e2);
                    break;
                }
            }
        }
        return edges;
    }

    public static bool[] ToBools(this List<Edge> l, MeshData md)
    {
        bool[] b = new bool[md.verts.Count];
        foreach (Edge e in l)
        {
            if (e.i1 < b.Length) b[e.i1] = true;
            if (e.i2 < b.Length) b[e.i2] = true;
        }
        return b;
    }

    public static int[] ToInts(this List<Edge> l, MeshData md)
    {
        return md.VertBoolsToIds(l.ToBools(md));
    }

    public static int[] ToBoundaryInts(this List<Edge> l)
    {
        List<int> l1 = new List<int>();
        foreach(Edge e in l)
        {
            l1.Add(e.i1);
        }
        return l1.ToArray();
    }
}


[Serializable]
public class Edge
{
    public int i1;
    public int i2;
    public int triangleIndex;

    public Edge(int ii1, int ii2, int ti)
    {
        i1 = ii1;
        i2 = ii2;
        triangleIndex = ti;
    }

    public override bool Equals(object obj)
    {
        if (obj is Edge)
        {
            Edge e = (Edge)obj;
            return e.i1 == i1 && e.i2 == i2 || e.i1 == i2 && e.i2 == i1;
        }

        return ReferenceEquals(this, obj);
    }

    public void SwapVerts()
    {
        int i0 = i2;
        i2 = i1;
        i1 = i0;
    }

    public override int GetHashCode()
    {
        var hashCode = -1565219140;
        hashCode = hashCode * -1521134295 + i1.GetHashCode();
        hashCode = hashCode * -1521134295 + i2.GetHashCode();
        hashCode = hashCode * -1521134295 + triangleIndex.GetHashCode();
        return hashCode;
    }
}

[Serializable]
public class Triangle
{
    public int i1;
    public int i2;
    public int i3;
    public int triIndex;

    public Triangle(int ii1, int ii2, int ii3, int ti)
    {
        i1 = ii1;
        i2 = ii2;
        i3 = ii3;
        triIndex = ti;
    }

    public Edge[] Edges
    {
        get { return new Edge[] { new Edge(i1, i2, triIndex), new Edge(i2, i3, triIndex), new Edge(i1, i3, triIndex) }; }
    }

    public bool HasVert(int i)
    {
        return i1 == i || i2 == i || i3 == i;
    }

    public override bool Equals(object obj)
    {
        if (obj is Triangle)
        {
            Triangle e = (Triangle)obj;
            return e.i1 == i1 && (e.i2 == i2 && e.i3 == i3 || e.i2 == i3 && e.i3 == i2) ||
                    e.i2 == i2 && (e.i1 == i1 && e.i3 == i3 || e.i1 == i3 && e.i3 == i1) ||
                    e.i3 == i3 && (e.i1 == i1 && e.i2 == i2 || e.i1 == i2 && e.i2 == i2);
        }

        return ReferenceEquals(this, obj);
    }

    public override int GetHashCode()
    {
        var hashCode = -1887938856;
        hashCode = hashCode * -1521134295 + i1.GetHashCode();
        hashCode = hashCode * -1521134295 + i2.GetHashCode();
        hashCode = hashCode * -1521134295 + i3.GetHashCode();
        hashCode = hashCode * -1521134295 + triIndex.GetHashCode();
        return hashCode;
    }
}

[Serializable]
public class Polygon
{
    static float caf = 0.4f;
    public Color colorAdd = new Color((UnityEngine.Random.value - 0.5f) * caf,
                                      (UnityEngine.Random.value - 0.5f) * caf,
                                      (UnityEngine.Random.value - 0.5f) * caf);
    public List<Triangle> triangles;

    public Polygon(Triangle init)
    {
        triangles = new List<Triangle>
        {
            init
        };
    }

    public bool AddAdjacentTriangle(Triangle triangle)
    {
        if (triangles.Contains(triangle)) return false;
        bool canAdd = false;
        foreach (Triangle t in triangles)
        {
            if (AdjacentTriangle(t, triangle))
            {
                canAdd = true;
                break;
            }
        }
        if (canAdd)
        {
            triangles.Add(triangle);
            return true;
        }
        return false;
    }

    public bool AdjacentTriangle(Triangle t1, Triangle t2)
    {
        Edge[] es1 = t1.Edges;
        Edge[] es2 = t2.Edges;
        foreach (Edge e1 in es1)
        {
            foreach (Edge e2 in es2)
            {
                if (e1.Equals(e2)) return true;
            }
        }
        return false;
    }

    public int[] GetVerts()
    {
        HashSet<int> l = new HashSet<int>();
        foreach (Triangle t in triangles)
        {
            l.Add(t.i1);
            l.Add(t.i2);
            l.Add(t.i3);
        }
        int[] iis = new int[l.Count];
        l.CopyTo(iis);
        return iis;
    }
}