﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public static class GenMeshPipe
{
    public static Mesh GeneratePipe(GenMesh gm)
    {
        Vector3 pos1 = new Vector3(0, -gm.size.y / 2, 0);
        Vector3 end1norm = Vector3.down;
        Vector3 end2norm = Vector3.up;
        Vector3 spin1 = Vector3.Cross(Vector3.back, end1norm).normalized;
        Vector3 spin2 = Vector3.Cross(end2norm, Vector3.back).normalized;
        float outerSize = gm.size.x;
        float innerSize = gm.size.z;

        MeshData outer = Cylinder(pos1, -pos1, outerSize, outerSize, end1norm, end2norm, Vector3.back,
                                  gm.pipeSlices, gm.pipeStacks, false, Vector2.zero, Vector2.one);
        MeshData inner = Cylinder(pos1, -pos1, innerSize, innerSize, end1norm, end2norm, Vector3.back,
                                  gm.pipeSlices, gm.pipeStacks, false, Vector2.zero, Vector2.one);
        inner.InvertNormals();
        inner.InvertTris();

        PipeXSection xSection1 = new PipeXSection(pos1, outerSize, end1norm, spin1);
        PipeXSection xSection2 = new PipeXSection(-pos1, outerSize, end2norm, spin2);
        xSection1.innerSize = xSection2.innerSize = innerSize;
        MeshData holedCap1 = HoledCap(xSection1, gm.pipeSlices, new Vector2(0.5f, 0), new Vector2(1, 0.5f));
        MeshData holedCap2 = HoledCap(xSection2, gm.pipeSlices, new Vector2(0.5f, 0.5f), new Vector2(1, 1));

        outer.Add(inner);
        outer.Add(holedCap1);
        outer.Add(holedCap2);

        Mesh m = outer.ToMesh("WG-Pipe");
        m.RecalculateBounds();
        //m.RecalculateNormals();
        m.RecalculateTangents();

        return m;
    }

    public static Mesh GenerateCylinder(GenMesh gm)
    {
        Vector3 pos1 = new Vector3(0, -gm.size.y / 2, 0);
        float size = gm.size.x;
        Vector3 end1norm = Vector3.down;
        Vector3 end2norm = Vector3.up;
        MeshData result = Cylinder(pos1, -pos1, size, size, end1norm, end2norm, Vector3.back,
                                  gm.cylinderSlices, gm.cylinderStacks, true, Vector2.zero, Vector2.one);

        Mesh m = result.ToMesh("WG-Cylinder");
        m.RecalculateBounds();
        //m.RecalculateNormals();
        m.RecalculateTangents();

        return m;
    }

    public static Mesh GenerateTorus(GenMesh gm)
    {
        float rIn = gm.torusInnerR;
        float rOut = gm.torusOuterR;
        int stacks = gm.torusStacks;
        int slices = gm.torusSlices;
        float rotAngle = 360f / stacks;

        PipeXSection[] pxes = new PipeXSection[stacks + 1];

        Vector3 spin = new Vector3(0, 0, -rIn);
        for (int i = 0; i < stacks + 1; i++)
        {
            Quaternion qu = Quaternion.Euler(0, rotAngle * i, 0);
            Vector3 pos = qu * spin;
            Vector3 dir0 = new Vector3(0, 1, 0);
            Vector3 norm = Vector3.Cross(dir0, pos);
            PipeXSection pxe = new PipeXSection(pos, rOut, norm, dir0);
            pxes[i] = pxe;
        }

        MeshData md = FromXSections(pxes, slices, Vector2.zero, Vector2.one);

        return md.ToMesh("WG-Torus");
    }

    public static Mesh GenerateSpiral(GenMesh gm)
    {
        float size1 = gm.spiralSize1.x;
        float size2 = gm.spiralSize2.x;
        float radius1 = gm.spiralSize1.y;
        float radius2 = gm.spiralSize2.y;

        int stacks = gm.spiralStacks;
        int slices = gm.spiralSlices;
        float rotAngle = gm.spiralAngles / stacks;
        float shiftDist = gm.spiralHeight / stacks;

        PipeXSection[] pxes = new PipeXSection[stacks + 1];

        Vector3 spin = new Vector3(0, 0, -1);
        for (int i = 0; i < stacks + 1; i++)
        {
            float ratio = (float)i / stacks;
            float size = size1 + (size2 - size1) * ratio;
            float radius = radius1 + (radius2 - radius1) * ratio;

            Quaternion qu = Quaternion.Euler(0, rotAngle * i, 0);
            Vector3 pos = (qu * spin) * size;
            Vector3 dir0 = new Vector3(0, 1, 0);
            Vector3 norm = Vector3.Cross(dir0, pos);
            // 0 => 1/10
            // 1 => 10
            float hnp = Mathf.Pow(10, 1f - gm.spiralHeightRatio * 2);
            float h = gm.spiralHeight * Mathf.Pow(ratio, hnp);
            pos += new Vector3(0, h, 0);
            PipeXSection pxe = new PipeXSection(pos, radius, norm, dir0);
            pxes[i] = pxe;
        }
        MeshData md = FromXSections(pxes, slices, Vector2.zero, new Vector2(0.5f, 1));

        // caps
        Vector3 pos1 = spin * size1;
        Vector3 dir1 = new Vector3(0, 1, 0);
        Vector3 norm1 = Vector3.Cross(pos1, dir1);
        PipeXSection cap1 = new PipeXSection(pos1, radius1, norm1, dir1);
        md.Add(Cap(cap1, slices, new Vector2(0.5f, 0), new Vector2(1, 0.5f)));

        Vector3 pos2 = (Quaternion.Euler(0, gm.spiralAngles, 0) * spin) * size2 + new Vector3(0, gm.spiralHeight, 0);
        Vector3 dir2 = new Vector3(0, 1, 0);
        Vector3 norm2 = Vector3.Cross(dir2, pos2);
        PipeXSection cap2 = new PipeXSection(pos2, radius2, norm2, dir2);
        md.Add(Cap(cap2, slices, new Vector2(0.5f, 0.5f), new Vector2(1, 1)));

        return md.ToMesh("WG-Spiral");
    }

    private static MeshData Cylinder(Vector3 end1, Vector3 end2, float size1, float size2, Vector3 norm1, Vector3 norm2, Vector3 axis,
                                   int slices, int stacks, bool cap, Vector2 uvMin, Vector2 uvMax)
    {
        MeshData md = new MeshData();
        float sliceAngle = 360f / slices;
        Vector3 spin1 = Vector3.Cross(axis, norm1).normalized;
        Vector3 spin2 = Vector3.Cross(norm2, axis).normalized;

        PipeXSection[] xSections = new PipeXSection[stacks + 1];
        if (cap)
        {
            PipeXSection xSection1 = new PipeXSection(end1, size1, norm1, spin1);
            md.Add(Cap(xSection1, slices, TransUV(uvMin, uvMax, 0.5f, 0), TransUV(uvMin, uvMax, 1, 0.5f)));
            PipeXSection xSection2 = new PipeXSection(end2, size2, norm2, spin2);
            md.Add(Cap(xSection2, slices, TransUV(uvMin, uvMax, 0.5f, 0.5f), TransUV(uvMin, uvMax, 1, 1)));
        }

        Vector3 e12normalized = (end2 - end1).normalized;
        for (int j = 0; j < stacks + 1; j++)
        {
            PipeXSection x = new PipeXSection(end1 + (end2 - end1) / stacks * j, size1 + (size2 - size1) / stacks * j, e12normalized, spin1);
            xSections[j] = x;
        }
        md.Add(FromXSections(xSections, slices, uvMin, TransUV(uvMin, uvMax, 0.5f, 1)));

        return md;
    }

    private static MeshData Cap(PipeXSection xSection, int slices, Vector2 uvMin, Vector2 uvMax)
    {
        MeshData md = new MeshData();
        float sliceAngle = 360f / slices;

        md.verts.Add(xSection.center);
        md.uvs.Add(TransUV(uvMin, uvMax, 0.5f, 0.5f));
        md.norms.Add(xSection.norm);
        int vt = 1;

        for (int i = 0; i < slices + 1; i++)
        {
            float rotAngle = sliceAngle * i;
            Quaternion qu = Quaternion.AngleAxis(rotAngle, xSection.norm);
            Quaternion uvqu = Quaternion.Euler(0, 0, -rotAngle + 90);
            Vector3 pos = xSection.center + (qu * xSection.spin) * xSection.size;
            Vector2 uv = new Vector2(0.5f, 0.5f) + (Vector2)(uvqu * new Vector2(0, 0.5f));
            uv = TransUV(uvMin, uvMax, uv.x, uv.y);
            md.verts.Add(pos);
            md.norms.Add(xSection.norm);
            md.uvs.Add(uv);
            if (i != slices)
            {
                md.tris.Add(vt);
                md.tris.Add(vt + 1);
                md.tris.Add(0);
            }
            vt++;
        }

        return md;
    }

    private static MeshData HoledCap(PipeXSection xSection, int slices, Vector2 uvMin, Vector2 uvMax)
    {
        MeshData md = new MeshData();
        float sliceAngle = 360f / slices;

        int vt = 0;

        for (int i = 0; i < slices + 1; i++)
        {
            float rotAngle = sliceAngle * i;
            Quaternion qu = Quaternion.AngleAxis(rotAngle, xSection.norm);
            Quaternion uvqu = Quaternion.Euler(0, 0, -rotAngle + 90);

            Vector3 posIn = xSection.center + (qu * xSection.spin) * xSection.innerSize;
            Vector3 posOut = xSection.center + (qu * xSection.spin) * xSection.size;

            Vector2 uvIn = new Vector2(0.5f, 0.5f) + (Vector2)(uvqu * new Vector2(0, 0.5f));
            Vector2 uvOut = new Vector2(0.5f, 0.5f) + (Vector2)(uvqu * new Vector2(0, 0.5f) * xSection.innerSize / xSection.size);
            uvIn = TransUV(uvMin, uvMax, uvIn.x, uvIn.y);
            uvOut = TransUV(uvMin, uvMax, uvOut.x, uvOut.y);

            md.verts.Add(posIn);
            md.norms.Add(xSection.norm);
            md.uvs.Add(uvIn);
            md.verts.Add(posOut);
            md.norms.Add(xSection.norm);
            md.uvs.Add(uvOut);

            if (i != slices)
            {
                md.tris.Add(vt);
                md.tris.Add(vt + 1);
                md.tris.Add(vt + 3);
                md.tris.Add(vt);
                md.tris.Add(vt + 3);
                md.tris.Add(vt + 2);
            }
            vt += 2;
        }

        return md;
    }

    private static MeshData FromXSections(PipeXSection[] xSections, int slices, Vector2 uvMin, Vector2 uvMax)
    {
        MeshData md = new MeshData();
        int vt = 0;
        int sectionCount = xSections.Length;
        float sliceAngle = 360f / slices;

        for (int i = 0; i < sectionCount; i++)
        {
            PipeXSection xSection = xSections[i];
            for (int j = 0; j < slices + 1; j++)
            {
                float rotAngle = -sliceAngle * j;
                Quaternion qu = Quaternion.AngleAxis(rotAngle, xSection.norm);
                Vector3 norm = (qu * xSection.spin);
                Vector3 pos = xSection.center + norm * xSection.size;
                Vector2 uv = new Vector2((float)j / slices, (float)i / (sectionCount - 1));
                uv = TransUV(uvMin, uvMax, uv.x, uv.y);
                md.verts.Add(pos);
                md.norms.Add(norm);
                md.uvs.Add(uv);
                if (j != slices && i != sectionCount - 1)
                {
                    md.tris.Add(vt);
                    md.tris.Add(vt + slices + 1);
                    md.tris.Add(vt + 1);
                    md.tris.Add(vt + 1);
                    md.tris.Add(vt + slices + 1);
                    md.tris.Add(vt + slices + 2);
                }
                vt++;
            }
        }
        return md;
    }

    private static Vector2 TransUV(Vector2 uvMin, Vector2 uvMax, float u, float v)
    {
        return new Vector2((uvMax.x - uvMin.x) * u + uvMin.x, (uvMax.y - uvMin.y) * v + uvMin.y);
    }

    static int MSSHK = 883215;
}

public class PipeXSection
{
    public Vector3 center;
    public float size;
    public float innerSize;
    public Vector3 norm;
    public Vector3 spin;

    public PipeXSection(Vector3 c, float s, Vector3 n, Vector3 d)
    {
        center = c;
        size = s;
        norm = n;
        spin = d;
    }
}