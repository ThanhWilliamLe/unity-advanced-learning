﻿using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

[RequireComponent(typeof(MeshFilter))]
public class GenMesh : MonoBehaviour
{
    public enum MeshType
    {
        Box, UVSphere, OctaSphere, IcoSphere, Cylinder, Pipe, Torus, Spiral, Cone, Poly
    }

    MeshFilter meshFilter;

    //Settings
    public MeshType meshType;
    public Vector3 size = Vector3.one;

    public int cubeDivX = 1;
    public int cubeDivY = 1;
    public int cubeDivZ = 1;

    public int sphereStacks = 2;
    public int sphereSlices = 3;
    public int sphereRecursion = 3;

    public int cylinderStacks = 1;
    public int cylinderSlices = 3;

    public int pipeStacks = 1;
    public int pipeSlices = 3;

    public float torusInnerR = 1;
    public float torusOuterR = 0.25f;
    public int torusStacks = 3;
    public int torusSlices = 3;

    public int spiralStacks = 8;
    public int spiralSlices = 3;
    public float spiralAngles = 360;
    public float spiralHeight = 1;
    public float spiralHeightRatio = 0.5f;
    public Vector2 spiralSize1 = new Vector2(1, 0.25f);
    public Vector2 spiralSize2 = new Vector2(1, 0.25f);

    void Start()
    {
        meshFilter = gameObject.GetComponent<MeshFilter>();
    }

    public void GenerateMesh()
    {
        if (meshFilter == null)
            meshFilter = gameObject.GetComponent<MeshFilter>();

        Mesh generatedMesh = null;
        switch (meshType)
        {
            case MeshType.Box: generatedMesh = GenMeshBox.Generate(this); break;
            case MeshType.UVSphere: generatedMesh = GenMeshSphere.Generate(this); break;
            case MeshType.OctaSphere: generatedMesh = GenMeshOctaSphere.Generate(this); break;
            case MeshType.IcoSphere: generatedMesh = GenMeshIcoSphere.Generate(this); break;
            case MeshType.Cylinder: generatedMesh = GenMeshPipe.GenerateCylinder(this); break;
            case MeshType.Pipe: generatedMesh = GenMeshPipe.GeneratePipe(this); break;
            case MeshType.Torus: generatedMesh = GenMeshPipe.GenerateTorus(this); break;
            case MeshType.Spiral: generatedMesh = GenMeshPipe.GenerateSpiral(this); break;
        }

        Mesh = generatedMesh;
        Resources.UnloadUnusedAssets();
    }

#if UNITY_EDITOR
    public void OptimizeMesh()
    {
        if (meshFilter == null)
            meshFilter = gameObject.GetComponent<MeshFilter>();

        if (Helpers.isEditor) MeshUtility.Optimize(meshFilter.sharedMesh);
        else MeshUtility.Optimize(meshFilter.mesh);
    }

    public void SaveMesh()
    {
        MeshSaver.SaveMesh(Mesh, Mesh.name, true, true);
    }
#endif

    public Mesh Mesh
    {
        get
        {
            if (meshFilter == null)
                meshFilter = gameObject.GetComponent<MeshFilter>();
            return Helpers.isEditor ? meshFilter.sharedMesh : meshFilter.mesh;
        }
        set
        {
            if (meshFilter == null) meshFilter = gameObject.GetComponent<MeshFilter>();
            if (Helpers.isEditor) meshFilter.sharedMesh = value;
            else meshFilter.mesh = value;
        }
    }

    public int SizeDimensions
    {
        get
        {
            if (meshType == MeshType.Torus) return 0;
            if (meshType == MeshType.Spiral) return 0;
            if (meshType == MeshType.IcoSphere) return 1;
            if (meshType == MeshType.OctaSphere) return 1;
            if (meshType == MeshType.Cylinder) return 2;
            return 3;
        }
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(GenMesh))]
public class GenMeshEditor : Editor
{
    GenMesh[] ts;
    bool autoGenMesh;
    bool showNormals;
    bool showTangents;
    bool showSettings = true;

    public override void OnInspectorGUI()
    {
        GenMesh t = (GenMesh)target;

        EditorGUI.BeginChangeCheck();

        GUILayout.Space(10);
        if (GUILayout.Button("Generate mesh")) t.GenerateMesh();
        if (GUILayout.Button("Optimize mesh")) t.OptimizeMesh();
        if (GUILayout.Button("Save mesh")) t.SaveMesh();
        autoGenMesh = GUILayout.Toggle(autoGenMesh, "Auto generate mesh");

        GUILayout.BeginHorizontal();
        GUILayout.Label("Visualization");
        showNormals = GUILayout.Toggle(showNormals, "normals");
        showTangents = GUILayout.Toggle(showTangents, "tangents");
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label("Mesh");
        GUILayout.Label(string.Format("Verts: {0}", t.Mesh != null ? t.Mesh.vertexCount : 0));
        GUILayout.Label(string.Format("Tris: {0}", t.Mesh != null ? t.Mesh.triangles.Length / 3 : 0));
        GUILayout.EndHorizontal();

        EditorGUILayout.Separator();
        GUILayout.Space(10);
        showSettings = EditorGUILayout.Foldout(showSettings, "Mesh settings", true);
        if (showSettings)
        {
            EditorGUI.indentLevel++;

            t.meshType = (GenMesh.MeshType)EditorGUILayout.EnumPopup("Mesh type", t.meshType);
            if (t.SizeDimensions == 1)
            {
                float size = EditorGUILayout.FloatField("Mesh size", t.size.x);
                t.size = new Vector3(size, size, size);
            }
            else if (t.SizeDimensions == 2)
            {
                Vector2 vsize = EditorGUILayout.Vector2Field("Mesh size", new Vector2(t.size.x, t.size.y));
                t.size = new Vector3(vsize.x, vsize.y, vsize.x);
            }
            else if (t.SizeDimensions >= 3) t.size = EditorGUILayout.Vector3Field("Mesh size", t.size);

            GUILayout.Space(10);
            EditorGUILayout.LabelField(t.meshType.ToString() + " configs");
            EditorGUI.indentLevel++;
            switch (((GenMesh)target).meshType)
            {
                case GenMesh.MeshType.Box:
                    t.cubeDivX = EditorGUILayout.IntSlider("X divisions", t.cubeDivX, 1, 64);
                    t.cubeDivY = EditorGUILayout.IntSlider("Y divisions", t.cubeDivY, 1, 64);
                    t.cubeDivZ = EditorGUILayout.IntSlider("Z divisions", t.cubeDivZ, 1, 64);
                    break;
                case GenMesh.MeshType.UVSphere:
                    t.sphereStacks = EditorGUILayout.IntSlider("Stacks", t.sphereStacks, 2, 64);
                    t.sphereSlices = EditorGUILayout.IntSlider("Slices", t.sphereSlices, 3, 64);
                    break;
                case GenMesh.MeshType.OctaSphere:
                    t.sphereRecursion = EditorGUILayout.IntSlider("Recursion", t.sphereRecursion, 1, 5);
                    break;
                case GenMesh.MeshType.IcoSphere:
                    t.sphereRecursion = EditorGUILayout.IntSlider("Recursion", t.sphereRecursion, 1, 5);
                    break;
                case GenMesh.MeshType.Cylinder:
                    t.cylinderStacks = EditorGUILayout.IntSlider("Stacks", t.cylinderStacks, 1, 64);
                    t.cylinderSlices = EditorGUILayout.IntSlider("Slices", t.cylinderSlices, 2, 64);
                    break;
                case GenMesh.MeshType.Pipe:
                    t.pipeStacks = EditorGUILayout.IntSlider("Stacks", t.pipeStacks, 1, 64);
                    t.pipeSlices = EditorGUILayout.IntSlider("Slices", t.pipeSlices, 2, 64);
                    break;
                case GenMesh.MeshType.Torus:
                    t.torusInnerR = EditorGUILayout.FloatField("Inner radius", t.torusInnerR);
                    t.torusOuterR = EditorGUILayout.FloatField("Outer radius", t.torusOuterR);
                    t.torusStacks = EditorGUILayout.IntSlider("Stacks", t.torusStacks, 3, 64);
                    t.torusSlices = EditorGUILayout.IntSlider("Slices", t.torusSlices, 2, 64);
                    break;
                case GenMesh.MeshType.Spiral:
                    t.spiralStacks = EditorGUILayout.IntSlider("Stacks", t.spiralStacks, 1, 1024);
                    t.spiralSlices = EditorGUILayout.IntSlider("Slices", t.spiralSlices, 2, 32);
                    t.spiralAngles = EditorGUILayout.FloatField("Angles", t.spiralAngles);
                    t.spiralHeight = EditorGUILayout.FloatField("Height", t.spiralHeight);
                    t.spiralHeightRatio = EditorGUILayout.Slider("Height ratio", t.spiralHeightRatio, 0, 1);
                    t.spiralSize1 = EditorGUILayout.Vector2Field("Size 1", t.spiralSize1);
                    t.spiralSize2 = EditorGUILayout.Vector2Field("Size 2", t.spiralSize2);
                    break;
            }

            EditorGUI.indentLevel--;
            EditorGUI.indentLevel--;
        }

        if (EditorGUI.EndChangeCheck())
        {
            if (GUI.changed && autoGenMesh)
            {
                t.GenerateMesh();
            }
        }
    }

    void OnSceneGUI()
    {
        GenMesh t = (GenMesh)target;
        if (t == null || t.Mesh == null) return;

        Handles.matrix = t.gameObject.transform.localToWorldMatrix;
        Handles.zTest = UnityEngine.Rendering.CompareFunction.LessEqual;

        int vertsCount = t.Mesh.vertexCount;
        for (int i = 0; i < vertsCount; i++)
        {
            if (i >= 5000) break;
            Vector3 start = t.Mesh.vertices[i];

            if (false)
            {
                Handles.color = Color.blue;
                Handles.Label(t.Mesh.vertices[i], i.ToString());
            }
            if (showNormals && t.Mesh.normals != null && t.Mesh.normals.Length > i)
            {
                Handles.color = Color.yellow;
                Handles.DrawLine(start, start + t.Mesh.normals[i]);
            }
            if (showTangents && t.Mesh.tangents != null && t.Mesh.tangents.Length > i)
            {
                Handles.color = Color.red;
                Vector3 tang = t.Mesh.tangents[i] * -t.Mesh.tangents[i].w;

                if (t.Mesh.normals != null && t.Mesh.normals.Length > i)
                    start += t.Mesh.normals[i] * 0.1f;

                Handles.DrawLine(start, start + tang);
            }
        }
    }

}

public static class MeshSaver
{

    [MenuItem("CONTEXT/MeshFilter/Save Mesh...")]
    public static void SaveMeshInPlace(MenuCommand menuCommand)
    {
        MeshFilter mf = menuCommand.context as MeshFilter;
        Mesh m = mf.sharedMesh;
        SaveMesh(m, m.name, false, true);
    }

    [MenuItem("CONTEXT/MeshFilter/Save Mesh As New Instance...")]
    public static void SaveMeshNewInstanceItem(MenuCommand menuCommand)
    {
        MeshFilter mf = menuCommand.context as MeshFilter;
        Mesh m = mf.sharedMesh;
        SaveMesh(m, m.name, true, true);
    }

    public static void SaveMesh(Mesh mesh, string name, bool makeNewInstance, bool optimizeMesh)
    {
        string path = EditorUtility.SaveFilePanel("Save Separate Mesh Asset", "Assets/", name, "asset");
        if (string.IsNullOrEmpty(path)) return;

        path = FileUtil.GetProjectRelativePath(path);

        Mesh meshToSave = (makeNewInstance) ? Object.Instantiate(mesh) as Mesh : mesh;

        if (optimizeMesh)
            MeshUtility.Optimize(meshToSave);

        AssetDatabase.CreateAsset(meshToSave, path);
        AssetDatabase.SaveAssets();
    }

}
#endif
