﻿using System;
using System.Collections.Generic;
using UnityEngine;

public static class ModifyMeshStellarize
{
    public static Mesh StellarizePoint(ModifyMesh mm, Mesh m)
    {
        MeshData md = new MeshData(m);

        return md.ToMesh(m.name);
    }

    public static Mesh StellarizeEdge(ModifyMesh mm, Mesh m)
    {
        MeshData md = new MeshData(m);

        return md.ToMesh(m.name);
    }

    public static Mesh StellarizeTri(ModifyMesh mm, Mesh m, Func<int, bool> stelCheck)
    {
        MeshData md = new MeshData(m);
        List<int> trisToRemove = new List<int>();
        Triangle[] tris = md.GetAllTriangles();
        for (int i = 0; i < tris.Length; i++)
        {
            if (!stelCheck(i)) continue;
            Triangle tri = tris[i];
            Vector3 center = (md.verts[tri.i1] + md.verts[tri.i2] + md.verts[tri.i3]) / 3;
            Vector3 normal = Vector3.Cross(md.verts[tri.i2] - md.verts[tri.i1], md.verts[tri.i3] - md.verts[tri.i1]).normalized;
            Vector2 uv = (md.uvs[tri.i1] + md.uvs[tri.i2] + md.uvs[tri.i3]) / 3;
            float length = mm.stellarizeLength * (mm.stellarizeLengthMin + (1 - mm.stellarizeLengthMin) * UnityEngine.Random.value);
            md.verts.Add(center + normal * length);
            md.norms.Add(normal);
            md.uvs.Add(uv);
            int i0 = md.verts.Count - 1;
            md.tris.Add(i0);
            md.tris.Add(tri.i1);
            md.tris.Add(tri.i2);
            md.tris.Add(i0);
            md.tris.Add(tri.i2);
            md.tris.Add(tri.i3);
            md.tris.Add(i0);
            md.tris.Add(tri.i3);
            md.tris.Add(tri.i1);
            trisToRemove.Add(tri.triIndex);
        }
        trisToRemove.Sort();
        trisToRemove.Reverse();
        foreach (int i in trisToRemove)
        {
            md.tris.RemoveRange(i, 3);
        }

        return md.ToMesh(m.name);
    }
}
