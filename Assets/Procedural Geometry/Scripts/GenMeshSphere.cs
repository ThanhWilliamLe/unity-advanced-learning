﻿using System;
using System.Collections.Generic;
using UnityEngine;

public static class GenMeshSphere
{
    public static Mesh Generate(GenMesh gm)
    {
        Vector3 size = gm.size;
        int stacks = gm.sphereStacks;
        int slices = gm.sphereSlices;
        List<Vector3> verts = new List<Vector3>();
        List<Vector2> uvs = new List<Vector2>();
        List<Vector3> norms = new List<Vector3>();
        List<int> tris = new List<int>();
        int vThrough = 0;
        int fThrough = 0;

        float stackAngle = 180f / stacks;
        float sliceAngle = 360f / slices;
        Vector3 start = new Vector3(0, -1, 0);
        for (int i = 0; i < slices + 1; i++)
        {
            for (int j = 0; j < stacks + 1; j++)
            {
                float horzAngle = -sliceAngle * i;
                float vertAngle = stackAngle * j;
                Vector3 pos = Quaternion.Euler(0, horzAngle, 0) * (Quaternion.Euler(vertAngle, 0, 0) * start);
                Vector3 horz = new Vector3(pos.x, 0, pos.z);

                Vector3 vert = new Vector3(pos.x * size.x, pos.y * size.y, pos.z * size.z);
                verts.Add(vert);

                Vector3 norm = pos;
                if (size.x == 0 && size.y == 0 && size.z == 0 ||
                    size.x == 0 && size.y == 0 && size.z != 0 ||
                    size.x == 0 && size.y != 0 && size.z == 0 ||
                    size.x != 0 && size.y == 0 && size.z == 0
                   ) norm = Vector3.zero;
                else if (size.x == 0) norm = new Vector3(Math.Sign(pos.x), 0, 0);
                else if (size.y == 0) norm = new Vector3(0, Math.Sign(pos.x), 0);
                else if (size.z == 0) norm = new Vector3(0, 0, Math.Sign(pos.x));
                else
                {
                    norm.x /= size.x;
                    norm.y /= size.y;
                    norm.z /= size.z;
                }
                norms.Add(norm.normalized);

                float u = Vector3.Angle(horz, new Vector3(0, 0, -1)) / 180;
                float v = pos.y / 2 + 0.5f;
                if (1 - Math.Abs(pos.y) >= 0.0001 / size.sqrMagnitude) v = Vector3.Angle(pos, horz) / 180 * Math.Sign(pos.y) + 0.5f;
                uvs.Add(new Vector2(u, v));

                if (i != slices && j != stacks)
                {
                    fThrough += AddQuadTris(tris, vThrough, stacks + 1, j == 0 ? 1 : j == stacks - 1 ? 2 : 0);
                }
                vThrough++;
            }
        }
        Mesh m = new Mesh
        {
            name = "WG-Sphere",
            vertices = verts.ToArray(),
            uv = uvs.ToArray(),
            normals = norms.ToArray(),
            triangles = tris.ToArray()
        };

        m.RecalculateBounds();
        //m.RecalculateNormals();
        m.RecalculateTangents();
        return m;
    }

    // n: number of stacks + 1 
    // 0: normal, 1 = bottom of sphere, 2 = top of sphere
    // return tris added
    public static int AddQuadTris(List<int> tris, int i, int n, int type = 0)
    {
        if (type == 1)
        {
            tris.Add(i);
            tris.Add(i + 1);
            tris.Add(i + n + 1);
            return 1;
        }
        if (type == 2)
        {
            tris.Add(i);
            tris.Add(i + 1);
            tris.Add(i + n);
            return 1;
        }
        tris.Add(i);
        tris.Add(i + 1);
        tris.Add(i + n);
        tris.Add(i + 1);
        tris.Add(i + n + 1);
        tris.Add(i + n);
        return 2;
    }
}