﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class MeshData
{
    public List<Vector3> verts = new List<Vector3>();
    public List<Vector2> uvs = new List<Vector2>();
    public List<Vector3> norms = new List<Vector3>();
    public List<int> tris = new List<int>();

    public MeshData()
    {

    }

    public MeshData(Mesh m)
    {
        verts = new List<Vector3>(m.vertices);
        uvs = new List<Vector2>(m.uv);
        norms = new List<Vector3>(m.normals);
        tris = new List<int>(m.triangles);
    }

    public void InvertNormals()
    {
        for (int i = 0; i < norms.Count; i++)
        {
            norms[i] *= -1;
        }
    }

    public void InvertTris()
    {
        for (int i = 0; i < tris.Count; i+=3)
        {
            int v0 = tris[i + 1];
            tris[i + 1] = tris[i + 2];
            tris[i + 2] = v0;
        }
    }

    public void Add(MeshData md)
    {
        tris.AddRange(md.TrisShifted(verts.Count));
        verts.AddRange(md.verts);
        uvs.AddRange(md.uvs);
        norms.AddRange(md.norms);
    }

    public List<int> TrisShifted(int i0)
    {
        List<int> l = new List<int>();
        for (int i = 0; i < tris.Count; i++)
        {
            l.Add(tris[i] + i0);
        }
        return l;
    }

    public Mesh ToMesh(string name)
    {
        Mesh m = new Mesh
        {
            name = name,
            vertices = verts.ToArray(),
            uv = uvs.ToArray(),
            normals = norms.ToArray(),
            triangles = tris.ToArray()
        };
        return m;
    }

    public void ToMesh(Mesh m)
    {
        m.vertices = verts.ToArray();
        m.uv = uvs.ToArray();
        m.normals = norms.ToArray();
        m.triangles = tris.ToArray();
    }

    #region Optimization

    public void RemoveAllStandaloneVerts()
    {
        bool[] standalones = new bool[verts.Count];
        for (int i = 0; i < tris.Count; i++)
        {
            standalones[tris[i]] = true;
        }
        for (int i = standalones.Length - 1; i >= 0; i--)
        {
            if (!standalones[i]) RemoveVert(i);
        }
    }

    public void RemoveVert(int i)
    {
        verts.RemoveAt(i);
        uvs.RemoveAt(i);
        norms.RemoveAt(i);
        for (int i0 = 0; i0 < tris.Count; i0++)
        {
            int vertIndex = tris[i0];
            if (vertIndex >= i)
            {
                vertIndex -= 1;
                tris[i0] = vertIndex;
            }
        }
    }

    public void ReplaceTriVert(int i0, int i1)
    {
        for (int i = 0; i < tris.Count; i++)
        {
            if (tris[i] == i0) tris[i] = i1;
        }
    }

    public void MergeSimilars()
    {
        int i = 0;
        int j = 0;
        while (i < verts.Count)
        {
            j = 0;
            while (j < verts.Count)
            {
                if (j != i && verts[i].Equals(verts[j]) && uvs[i].Equals(uvs[j]) && norms[i].Equals(norms[j]))
                {
                    ReplaceTriVert(j, i);
                    RemoveVert(j);
                }
                j++;
            }
            i++;
        }
    }

    #endregion

    #region Modification

    public int DupeAndShift(int i, Vector3 shift)
    {
        verts.Add(verts[i] + shift);
        uvs.Add(uvs[i]);
        norms.Add(norms[i]);
        return verts.Count - 1;
    }

    public void Stich(int[] is1, int[] is2)
    {
        if (is1 == null || is2 == null || is1.Length <= 1 || is1.Length != is2.Length) return;

        Vector3 z = Vector3.zero;
        for (int i = 0; i < is1.Length; i++)
        {
            is1[i] = DupeAndShift(is1[i], z);
            is2[i] = DupeAndShift(is2[i], z);
        }
        for (int i = 0; i < is1.Length; i++)
        {
            Vector3 n = Vector3.zero;
            if (i < is1.Length - 1) n = Vector3.Cross(verts[is1[i + 1]] - verts[is1[i]], verts[is2[i]] - verts[is1[i]]);
            else n = Vector3.Cross(verts[is2[i]] - verts[is1[i]], verts[is1[i - 1]] - verts[is1[i]]);
            n.Normalize();
            norms[is1[i]] = n;
            norms[is2[i]] = n;
        }
        for (int i = 0; i < is1.Length - 1; i++)
        {
            AddQuadTri(is1[i], is1[i + 1], is2[i + 1], is2[i]);
        }
    }

    // 1->4 quad points clockwise
    public void AddQuadTri(int i1, int i2, int i3, int i4)
    {
        tris.Add(i1);
        tris.Add(i2);
        tris.Add(i3);
        tris.Add(i1);
        tris.Add(i3);
        tris.Add(i4);
    }

    #endregion


    #region Gets

    public bool[] VertIdsToBools(int[] ids)
    {
        bool[] b = new bool[verts.Count];
        foreach (int i in ids)
        {
            if (i < b.Length) b[i] = true;
        }
        return b;
    }

    public int[] VertBoolsToIds(bool[] b)
    {
        List<int> l = new List<int>();
        for (int i = 0; i < b.Length; i++)
        {
            if (b[i]) l.Add(i);
        }
        return l.ToArray();
    }

    public int[] GetInnerVerts(int[] ids)
    {
        int vc = verts.Count;
        bool[] innerVerts = new bool[vc];
        bool[] allVerts = VertIdsToBools(ids);

        List<Edge> boundaries = GetEdges(ids);
        boundaries.ToBoundaries();
        bool[] boundaryVerts = boundaries.ToBools(this);

        for (int i = 0; i < vc; i++)
        {
            innerVerts[i] = allVerts[i] && !boundaryVerts[i];
        }

        return VertBoolsToIds(innerVerts);
    }

    public List<Edge> GetEdges(int[] ids)
    {
        bool[] check = VertIdsToBools(ids);
        List<Edge> edges = new List<Edge>();
        for (int i = 0; i < tris.Count; i += 3)
        {
            int i1 = tris[i];
            int i2 = tris[i + 1];
            int i3 = tris[i + 2];
            if (check[i1] && check[i2] && check[i3])
            {
                edges.Add(new Edge(i1, i2, i));
                edges.Add(new Edge(i2, i3, i));
                edges.Add(new Edge(i1, i3, i));
            }
        }
        return edges;
    }

    public List<Edge> GetSharedEdges()
    {
        Dictionary<Edge, int> d = new Dictionary<Edge, int>();
        Triangle[] tris = GetAllTriangles();
        foreach (Triangle tri in tris)
        {
            Edge[] es = tri.Edges;
            foreach (Edge e in es)
            {
                if (!d.ContainsKey(e)) d.Add(e, 1);
                else d[e] += 1;
            }
        }
        foreach (Edge e in d.Keys)
        {
            if (d[e] < 2) d.Remove(e);
        }
        return new List<Edge>(d.Keys);
    }

    public int[] GetTriangleIndexes(int[] verts)
    {
        List<int> theTris = new List<int>();
        bool[] vertBools = VertIdsToBools(verts);
        for (int i = 0; i < tris.Count; i += 3)
        {
            int i1 = tris[i];
            int i2 = tris[i + 1];
            int i3 = tris[i + 2];
            if (vertBools[i1] && vertBools[i2] && vertBools[i3]) theTris.Add(i);
        }
        return theTris.ToArray();
    }

    public Triangle[] GetTriangles(int[] verts)
    {
        List<Triangle> theTris = new List<Triangle>();
        bool[] vertBools = VertIdsToBools(verts);
        for (int i = 0; i < tris.Count; i += 3)
        {
            int i1 = tris[i];
            int i2 = tris[i + 1];
            int i3 = tris[i + 2];
            if (vertBools[i1] && vertBools[i2] && vertBools[i3]) theTris.Add(new Triangle(i1, i2, i3, i));
        }
        return theTris.ToArray();
    }

    public Triangle[] GetAllTriangles()
    {
        Triangle[] ts = new Triangle[tris.Count / 3];
        for (int i = 0; i < tris.Count; i += 3)
        {
            ts[i / 3] = new Triangle(tris[i], tris[i + 1], tris[i + 2], i);
        }
        return ts;
    }

    public List<Polygon> GetPolys(int[] ids)
    {
        int safeCounter = 0;
        int safeCounterMax = ids.Length * ids.Length * ids.Length * ids.Length;

        List<Polygon> l = new List<Polygon>();
        List<Triangle> freeTriangles = new List<Triangle>(GetTriangles(ids));
        while (freeTriangles.Count > 0)
        {
            Polygon initPoly = new Polygon(freeTriangles[0]);
            freeTriangles.RemoveAt(0);

            bool aFreeTriAdded = true;
            while (aFreeTriAdded && safeCounter++ <= safeCounterMax)
            {
                aFreeTriAdded = false;
                List<Triangle> addedTris = new List<Triangle>();
                foreach (Triangle tri in freeTriangles)
                {
                    if (initPoly.AddAdjacentTriangle(tri))
                    {
                        addedTris.Add(tri);
                        aFreeTriAdded = true;
                        break;
                    }
                }
                foreach (Triangle tri in addedTris)
                {
                    freeTriangles.Remove(tri);
                }
            }
            l.Add(initPoly);
        }
        return l;
    }

    #endregion
}
