﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

#if UNITY_EDITOR
using UnityEditor;
#endif


[RequireComponent(typeof(MeshFilter))]
public class ModifyMesh : MonoBehaviour
{
    Stack<Mesh> meshHistory = new Stack<Mesh>();
    Stack<Mesh> meshFuture = new Stack<Mesh>();
    MeshFilter meshFilter;

    public float extrudeLength = 1;
    public float extrudeRatio = 1;
    public int extrudeSkips = 0;
    public bool extrudeUnidir = false;

    public float stellarizeLength = 1;
    public float stellarizeLengthMin = 1;
    public int stellarizeSkip = 0;
    public float stellarizeCount = 0;

    public Mesh Mesh
    {
        get
        {
            if (meshFilter == null) meshFilter = gameObject.GetComponent<MeshFilter>();
            return Helpers.isEditor ? meshFilter.sharedMesh : meshFilter.mesh;
        }
        set
        {
            if (meshFilter == null) meshFilter = gameObject.GetComponent<MeshFilter>();
            if (Helpers.isEditor) meshFilter.sharedMesh = value;
            else meshFilter.mesh = value;
        }
    }

    public void UpdateMesh(Mesh newMesh)
    {
        PreModifyMesh();
        Mesh = newMesh;
    }

    void PreModifyMesh()
    {
        AddHistory();
        ClearFuture();
    }

    void AddHistory()
    {
        Mesh m = Instantiate(Mesh);
        m.name = Mesh.name;
        meshHistory.Push(m);
    }

    void AddFuture()
    {
        Mesh m = Instantiate(Mesh);
        m.name = Mesh.name;
        meshFuture.Push(m);
    }

    void ClearFuture()
    {
        meshFuture.Clear();
    }

    public void Undo()
    {
        if (CanUndo())
        {
            AddFuture();
            Mesh = meshHistory.Pop();
        }
    }

    public bool CanUndo()
    {
        return meshHistory.Count > 0;
    }

    public void Redo()
    {
        if (CanRedo())
        {
            AddHistory();
            Mesh = meshFuture.Pop();
        }
    }

    public bool CanRedo()
    {
        return meshFuture.Count > 0;
    }

    public void ClearHistoryAndFuture()
    {
        ClearFuture();
        meshHistory.Clear();
    }
}


#if UNITY_EDITOR
[CustomEditor(typeof(ModifyMesh))]
public class ModifyMeshEditor : Editor
{
    public override void OnInspectorGUI()
    {
        ModifyMesh t = (ModifyMesh)target;

        EditorGUILayout.BeginHorizontal();
        GUI.enabled = t.CanUndo();
        if (GUILayout.Button("Undo")) t.Undo();
        GUI.enabled = t.CanRedo();
        if (GUILayout.Button("Redo")) t.Redo();
        GUI.enabled = true;
        EditorGUILayout.EndHorizontal();
        if (GUILayout.Button("Clear history")) t.ClearHistoryAndFuture();


        GUILayout.Space(20);
        EditorGUILayout.LabelField("Extrude", EditorStyles.boldLabel);
        EditorGUI.indentLevel++;
        t.extrudeLength = EditorGUILayout.FloatField("Length", t.extrudeLength);
        t.extrudeUnidir = EditorGUILayout.Toggle("Unidirection", t.extrudeUnidir);

        t.extrudeRatio = EditorGUILayout.Slider("Ratio", t.extrudeRatio, 0, 1);
        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("Extrude Random"))
        {
            t.UpdateMesh(ModifyMeshExtrude.ExtrudePolygons(t, t.Mesh, (int i) =>
            {
                return UnityEngine.Random.value <= t.extrudeRatio;
            }));
        }
        if (GUILayout.Button("Extrude Partial"))
        {
            t.UpdateMesh(ModifyMeshExtrude.ExtrudePolygons(t, t.Mesh, (int i) =>
            {
                return i <= t.Mesh.triangles.Length * t.extrudeRatio;
            }));
        }
        EditorGUILayout.EndHorizontal();

        t.extrudeSkips = EditorGUILayout.IntField("Skips", t.extrudeSkips);
        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("Extrude Tri Even"))
        {
            t.UpdateMesh(ModifyMeshExtrude.ExtrudeTriangles(t, t.Mesh, 1));
        }
        if (GUILayout.Button("Extrude Quad Even"))
        {
            t.UpdateMesh(ModifyMeshExtrude.ExtrudeTriangles(t, t.Mesh, 2));
        }
        EditorGUILayout.EndHorizontal();
        EditorGUI.indentLevel--;


        GUILayout.Space(20);
        EditorGUILayout.LabelField("Stellarize", EditorStyles.boldLabel);
        EditorGUI.indentLevel++;
        t.stellarizeLength = EditorGUILayout.FloatField("Length", t.stellarizeLength);
        t.stellarizeLengthMin = EditorGUILayout.Slider("Length Scale Min", t.stellarizeLengthMin, 0, 1);
        t.stellarizeSkip = EditorGUILayout.IntField("Skips", t.stellarizeSkip);
        t.stellarizeCount = EditorGUILayout.Slider("Count", t.stellarizeCount, 0, 1);
        if (GUILayout.Button("Stellarize Point"))
        {
            t.UpdateMesh(ModifyMeshStellarize.StellarizePoint(t, t.Mesh));
        }
        if (GUILayout.Button("Stellarize Edge"))
        {
            t.UpdateMesh(ModifyMeshStellarize.StellarizeEdge(t, t.Mesh));
        }
        if (GUILayout.Button("Stellarize Tri"))
        {
            t.UpdateMesh(ModifyMeshStellarize.StellarizeTri(t, t.Mesh, (int i) =>
            {
                return i % (Mathf.Max(0, t.stellarizeSkip) + 1) == 0 && UnityEngine.Random.value <= t.stellarizeCount;
            }));
        }
        EditorGUI.indentLevel--;
    }

}
#endif
