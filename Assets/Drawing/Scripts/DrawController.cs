﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawController : MonoBehaviour
{
    public LineRenderer lineRenderer;
    public DrawInterpolateType interpolateType;

    public void StartDrawing()
    {

    }

    public void DrawPoint(Vector3 vector)
    {
        print(vector);
    }

    public void StopDrawing()
    {

    }
}

public enum DrawInterpolateType
{
    None, Smooth, Sharp
}