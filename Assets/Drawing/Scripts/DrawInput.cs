﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class DrawInput : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public Camera cam;
    public Image cover;
    public DrawController drawController;

    bool drawing;

    void Start()
    {
        SetCoverAlpha(0);
    }

    public void OnBeginDrag(PointerEventData eventData) { StartDrawing(eventData); }
    public void OnDrag(PointerEventData eventData) { ContinueDrawing(eventData); }
    public void OnEndDrag(PointerEventData eventData) { EndDrawing(eventData); }

    void SetCoverAlpha(float a)
    {
        if (cover != null)
        {
            Color c = cover.color;
            c.a = a;
            cover.color = c;
        }
    }

    void StartDrawing(PointerEventData e)
    {
        SetCoverAlpha(0.25f);
        drawController.StartDrawing();
        drawController.DrawPoint(PointerWorldPos(e));
    }

    void ContinueDrawing(PointerEventData e)
    {
        drawController.DrawPoint(PointerWorldPos(e));
    }

    void EndDrawing(PointerEventData e)
    {
        SetCoverAlpha(0);
        drawController.StopDrawing();
    }

    Vector3 PointerWorldPos(PointerEventData e)
    {
        print("----");
        print(e.position);
        print(cam.ScreenToWorldPoint(e.position));
        return cam.ScreenToWorldPoint(e.position);
    }
}
