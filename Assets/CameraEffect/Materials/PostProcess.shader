﻿Shader "William/PostProcess" 
{
  Properties 
  {
    // texture
    _MainTex("Main Texture", 2D) = "white" {}

    // pre
    _Blur("Blur", Range(0, 10)) = 0
    _Distort("Distort", Range(-1,1)) = 0

    // basics
    _Brightness("Brightness", Range(-255, 255)) = 0
    _Contrast("Contrast", Range(-255, 255)) = 0

    // colors
    _Tint("Tint", Color) = (1,1,1,1)
    _Highlight("Highlight", Color) = (1,1,1,1)
    _Shadow("Shadow", Color) = (1,1,1,1)
    _SplitCenter("Split Color Center", Range(0,1)) = 0.5
    _SplitSoft("Split Color Softness", Range(0.001,0.999)) = 0.25

    // grain
    _GCount("Grain Count", Range(0, 1)) = 0
    _GStrength("Grain Strength", Range(-1, 1)) = 0
    _GSize("Grain Size", Range(0, 30)) = 0
    _GSpeed("Grain Alter Speed", Range(1, 9999)) = 1000
    [Toggle] _GCircle("Grain Circle", Float) = 0

    // vignette
    _VRadius("Vignette Radius", Range(0.0, 1.0)) = 1.0
    _VSoft("Vignette Softness", Range(0.0, 1.0)) = 0.5
    _VOffsetX("Vignette Offset X", Range(-2, 2)) = 0
    _VOffsetY("Vignette Offset Y", Range(-2, 2)) = 0
  }

  SubShader 
  {
    Pass 
    {
      CGPROGRAM
      #pragma vertex vert_img
      #pragma fragment frag
      #include "UnityCG.cginc" // required for v2f_img

      // Properties
      sampler2D _MainTex;

      float _Brightness, _Contrast;
      
      float4 _Tint, _Highlight, _Shadow;
      float _SplitCenter, _SplitSoft;

      float _Blur, _Distort;
      float _GCount, _GStrength, _GSize, _GSpeed, _GCircle;
      float _VRadius, _VSoft, _VOffsetX, _VOffsetY;


      //Methods
      float Rand3(float3 v) 
      {
        return frac(sin(_Time[0] * dot(v ,float3(12.9898,78.233,45.5432))) * 43758.5453);
      }

      float Rand2(float2 v, float speed) 
      {
        return frac(sin(round(_Time[0] * speed)/speed * dot(v ,float2(12.9898,78.233))) * 43758.5453);
      }

      float4 AlphaToStrength(float4 col)
      {
        float4 white = float4(1,1,1,1);
        return white + float4(col.rgb - white.xyz, 1) * col.a;
      }

      float Brightness(float4 col)
      {
        return (col.x+col.y+col.z) * 1 / 3;
      }

      float4 LerpBothWays(float4 a, float4 b, float4 x, float l, int p)
      {
        if(l<0.5)
          x = lerp(x, a, pow(1-l*2, p));
        else if(l>0.5)
          x = lerp(x, b, pow((l-0.5)*2, p));
        return x;
      }

      void ApplyPres(float4 pos, half2 uv, out half2 uvout, out float4 colorout)
      {
        // distort
        half2 centerToUv = (uv - 0.5);
        half r = length(centerToUv);
        if(r!=0)
        {
          float rd = r - sign(_Distort) * pow(abs(_Distort), abs(1-r));
          centerToUv = centerToUv / r * rd;
          uv = centerToUv + 0.5;
        }

        // blur
        float4 color = tex2D(_MainTex, uv);
        int count = 1;
        int maxX = floor(_Blur);
        int maxY = floor(_Blur);
        float totalColor = color;
        for(int i=-maxX ; i<=maxX ; i++)
        {
          for(int j=-maxY ; j<=maxY ; j++)
          {
            totalColor += tex2D(_MainTex, uv+float2(i / _ScreenParams.x, j / _ScreenParams.y));
            count += 1;
          }
        }
        color = totalColor / count;

        uvout = uv;
        colorout = color;
      }

      float4 ApplyBasics(float4 base, float brightness, float contrast)
      {
        // https://www.dfstudios.co.uk/articles/programming/image-programming-algorithms/image-processing-algorithms-part-4-brightness-adjustment/
        base += float4(brightness, brightness, brightness, 0) / 255;
        base = saturate(base);

        // http://thecryptmag.com/Online/56/imgproc_5.html
        float f = 259 * (contrast + 255) / 255 / (259 - contrast);
        float3 newB = f * (256 * base.xyz - 128) + 128;
        base  = float4(newB, base.w);
        base /= 256;

        return saturate(base);
      }

      float4 ApplyColors(float4 base)
      {
        float splitLeft = _SplitCenter - _SplitSoft;
        float splitRight = _SplitCenter + _SplitSoft;
        float4 highlight = float4(_Highlight.xyz, smoothstep(splitLeft, splitRight, Brightness(base))*_Highlight.w);
        float4 shadow = float4(_Shadow.xyz, smoothstep(splitRight, splitLeft, Brightness(base))*_Shadow.w);
        base *= AlphaToStrength(highlight);
        base *= AlphaToStrength(shadow);
        base *= AlphaToStrength(_Tint);
        return base;
      }

      float4 ApplyGrain(float4 color, half2 pos)
      {
        float xMult = _ScreenParams.x / _GSize;
        float yMult = _ScreenParams.y / _GSize;

        float2 posShift = float2(pos.x * xMult, pos.y * yMult);
        int2 grainPos = int2(posShift.x, posShift.y);
        float grainDist = distance(posShift, grainPos + 0.5);
        float grainFallOff = saturate(1 - grainDist * 2);
        if(_GCircle==0) grainFallOff = 1;

        float grainDef = Rand2(grainPos, _GSpeed);
        bool grainHere = grainDef < _GCount;
        float grainBase = grainDef / _GCount;

        if(grainHere)
        {
          float grainColor = - grainBase * grainFallOff * _GStrength;
          color += float4(grainColor, grainColor, grainColor, 0);
        }

        return saturate(color);
      }

      float4 ApplyVignette(float4 base, float2 pos)
      {
        float2 vignettePosition = pos - float2(_VOffsetX, _VOffsetY);
        float distFromCenter = distance(vignettePosition, float2(0.5, 0.5));
        float vignette = smoothstep(_VRadius, _VRadius - _VSoft, distFromCenter);
        return base*vignette;
      }

      float4 frag(v2f_img i) : COLOR 
      {
        float4 color = 0;
        half2 newuv = i.uv;
        ApplyPres(i.pos, i.uv, newuv, color);

        color = ApplyBasics(color, _Brightness, _Contrast);
        color = ApplyColors(color);
        color = ApplyGrain(color, i.uv);
        color = ApplyVignette(color, i.uv);

        return saturate(color);
      }

      ENDCG
    }
  }

  CustomEditor "PostProcessShaderGUI"

}