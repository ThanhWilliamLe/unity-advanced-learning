﻿using System;
using UnityEditor;
using UnityEngine;

public class PostProcessShaderGUI : ShaderGUI
{
    GUIStyle label0 = new GUIStyle()
    {
        fontSize = 14,
        fontStyle = FontStyle.Bold
    };

    GUIStyle label1 = new GUIStyle()
    {
        fontSize = 11,
        fontStyle = FontStyle.Italic
    };

    public override void OnGUI(MaterialEditor materialEditor, MaterialProperty[] properties)
    {
        //base.OnGUI(materialEditor, properties);
        //foreach (MaterialProperty p in properties)
        //{
        //    materialEditor.ShaderProperty(p, p.displayName);
        //}

        EditorGUI.BeginChangeCheck();
        {
            //GUILayout.Label("Textures", label0);
            //Show(materialEditor, properties, "_MainTex");

            Space(3);
            GUILayout.Label("Preeffects", label0);
            Show(materialEditor, properties, "_Blur");
            Show(materialEditor, properties, "_Distort");

            Space(3);
            GUILayout.Label("Basics", label0);
            Show(materialEditor, properties, "_Brightness");
            Show(materialEditor, properties, "_Contrast");


            Space(3);
            GUILayout.Label("Colors", label0);
            Show(materialEditor, properties, "_Tint");
            Space(1);
            GUILayout.Label("Split toning", label1);
            Show(materialEditor, properties, "_Highlight");
            Show(materialEditor, properties, "_Shadow");
            Show(materialEditor, properties, "_SplitCenter");
            Show(materialEditor, properties, "_SplitSoft");

            Space(3);
            GUILayout.Label("Vignette", label0);
            Show(materialEditor, properties, "_VRadius");
            Show(materialEditor, properties, "_VSoft");
            Show(materialEditor, properties, "_VOffsetX");
            Show(materialEditor, properties, "_VOffsetY");

            Space(3);
            GUILayout.Label("Grain", label0);
            Show(materialEditor, properties, "_GCount");
            Show(materialEditor, properties, "_GStrength");
            Show(materialEditor, properties, "_GSize");
            Show(materialEditor, properties, "_GSpeed");
            Show(materialEditor, properties, "_GCircle");
        }

    }

    public void Space(int i = 1)
    {
        for (int i0 = 0; i0 < i; i0++)
        {
            EditorGUILayout.Space();
        }
    }

    public void Show(MaterialEditor m, MaterialProperty[] ps, string findName)
    {
        MaterialProperty p = FindProperty(findName, ps);
        if (p != null)
        {
            m.ShaderProperty(p, p.displayName);
        }
    }
}