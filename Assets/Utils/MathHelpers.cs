﻿using System.Collections.Generic;
using UnityEngine;

public static class MathHelpers
{
    public static float Mod(float a, float b)
    {
        return a - b * Mathf.Floor(a / b);
    }

    public static float PowerLerp(float a, float b, float t, float p, bool clamped = true)
    {
        if (clamped) return Mathf.Lerp(a, b, Mathf.Pow(t, p));
        else return Mathf.LerpUnclamped(a, b, Mathf.Pow(t, p));
    }

    /*
     * Can be intensive 
     */
    public static float SmoothLerp(float a, float b, float t, float f = 2, bool clamped = true)
    {
        float tp = Mathf.Pow(t, f);
        t = tp / (tp + Mathf.Pow(1 - t, f));

        if (clamped) return Mathf.Lerp(a, b, t);
        else return Mathf.LerpUnclamped(a, b, t);
    }

    public static bool InBound(Vector3 v0, Vector3 v1, Vector3 v2)
    {
        Vector3 min = new Vector3(Mathf.Min(v1.x, v2.x), Mathf.Min(v1.y, v2.y), Mathf.Min(v1.z, v2.z));
        Vector3 max = new Vector3(Mathf.Max(v1.x, v2.x), Mathf.Max(v1.y, v2.y), Mathf.Max(v1.z, v2.z));
        return v0.x >= min.x && v0.y >= min.y && v0.z >= min.z
            && v0.x <= max.x && v0.y <= max.y && v0.z <= max.z;
    }
}

public struct Line
{
    public Vector3 point;
    public Vector3 dir;

    public Line(Vector3 p, Vector3 d)
    {
        point = p;
        dir = d;
    }

    public static bool OnLine(Line l1, Vector3 p)
    {
        Vector3 pdiff = p - l1.point;
        double xRatio = l1.dir.x == 0 ? 0 : pdiff.x / l1.dir.x;
        double yRatio = l1.dir.y == 0 ? 0 : pdiff.y / l1.dir.y;
        return xRatio == yRatio;
    }

    public static bool Parallel(Line l1, Line l2)
    {
        return Vector3.Cross(l1.dir, l2.dir).sqrMagnitude == 0;
    }

    public static bool Coplanar(Line l1, Line l2)
    {
        return Vector3.Dot(Vector3.Cross(l1.dir, l2.dir), l2.point - l1.point) == 0;
    }

    public static Vector3? Intersect(Line l1, Line l2)
    {
        if (Parallel(l1, l2) || !Coplanar(l1, l2)) return null;

        Vector3 p1p2 = l2.point - l1.point;
        Vector3 d1xd2 = Vector3.Cross(l1.dir, l2.dir);
        float a = Vector3.Cross(p1p2, l2.dir).magnitude / d1xd2.magnitude;

        return l1.point + a * l1.dir;
    }

    public static List<Vector3> IntersectLines(Line l, params Line[] ls)
    {
        List<Vector3> list = new List<Vector3>();
        foreach (Line l1 in ls)
        {
            Vector3? i1 = Intersect(l, l1);
            //Debug.Log(l1.point + "   " + l1.dir + "   " + i1);
            if (i1 != null && !list.Contains(i1.Value)) list.Add(i1.Value);
        }
        return list;
    }

    public static List<Vector3> IntersectRect(Line l, Rect rect)
    {
        Line l1 = new Line(rect.min, new Vector2(1, 0));
        Line l2 = new Line(rect.min, new Vector2(0, 1));
        Line l3 = new Line(rect.max, new Vector2(-1, 0));
        Line l4 = new Line(rect.max, new Vector2(0, -1));

        List<Vector3> list = IntersectLines(l, l1, l2, l3, l4);
        list.RemoveAll((v) => !MathHelpers.InBound(v, rect.min, rect.max));

        if (list.Count > 2) Debug.LogWarning("LINE INTERSECT RECT AT MORE THAN 2 POINTS. WEIRD!");
        return list;
    }

    public static Vector3? IntersectRectClosest(Line l, Rect rect, Vector3 reference)
    {
        List<Vector3> intersects = IntersectRect(l, rect);
        Vector3? v = null;
        foreach (Vector3 v0 in intersects)
        {
            if (!v.HasValue) v = v0;
            else if ((v.Value - reference).sqrMagnitude > (v0 - reference).sqrMagnitude) v = v0;
        }
        return v;
    }

    public static float DistanceSqr(Line l, Vector3 p)
    {
        if (OnLine(l, p)) return 0;

        Plane pl = new Plane(p, l.dir);
        Vector3? intersect = Plane.Intersect(pl, l);
        if (intersect != null) return (intersect.Value - p).sqrMagnitude;

        return -1;
    }
}

public struct Plane
{
    public Vector3 point;
    public Vector3 normal;

    public Plane(Vector3 p, Vector3 d)
    {
        point = p;
        normal = d;
    }

    public static bool OnPlane(Plane pl, Vector3 p)
    {
        Vector3 pdiff = p - pl.point;
        if (pdiff.sqrMagnitude == 0) return true;
        return Vector3.Dot(pdiff, pl.normal) == 0;
    }

    public static bool Parallel(Plane pl, Line l)
    {
        return Parallel(pl, l.dir);
    }

    public static bool Parallel(Plane pl, Vector3 p)
    {
        return Vector3.Dot(pl.normal, p) == 0;
    }

    public static bool Parallel(Plane p1, Plane p2)
    {
        return Mathf.Abs(Vector3.Dot(p1.normal, p2.normal)) == 1;
    }

    public static float DistanceSqr(Plane pl, Vector3 p)
    {
        Vector3 v1 = (pl.point - p).normalized;
        Vector3 v2 = (p + pl.normal).normalized;
        return Mathf.Abs(Vector3.Dot(v1, v2)) * (pl.point - p).sqrMagnitude;
    }

    public static Vector3? Intersect(Plane pl, Line l)
    {
        if (Parallel(pl, l)) return null;
        float k = (Vector3.Dot(pl.normal, pl.point) - Vector3.Dot(pl.normal, l.point)) / Vector3.Dot(pl.normal, l.dir);
        return null;
    }
}