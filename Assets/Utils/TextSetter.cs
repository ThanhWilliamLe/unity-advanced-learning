﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TextSetter : MonoBehaviour
{
    [TextArea]
    public string unformatted;
    public Text text;

    public void Apply(params object[] args)
    {
        text.text = string.Format(unformatted, args);
    }
}
