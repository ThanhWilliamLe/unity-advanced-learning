﻿using UnityEngine;
using System.Collections;
using System;

[CreateAssetMenu(menuName = "Game/Color Palette")]
public class ColorPalette : ScriptableObject
{
    public ColorGroup[] palette;
}

[Serializable]
public class ColorGroup
{
    public string name;
    public Color[] colors;
}