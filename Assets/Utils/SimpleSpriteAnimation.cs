﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleSpriteAnimation : MonoBehaviour
{
    public Component target;
    public List<Sprite> sprites;
    public float framesPerSecond = 30;
    public bool pingpong;

    int current;
    float timePassed;
    bool reversing;

    private void Start()
    {
        current = 0;
        NextSprite();
    }

    void Update()
    {
        timePassed += Time.deltaTime;
        if (timePassed > 1f / framesPerSecond)
        {
            timePassed -= 1f / framesPerSecond;
            NextSprite();
        }
    }

    void NextSprite()
    {
        if (sprites.Count > 0)
        {
            if (ObjectUtils.SetSprite(target, sprites[current]))
            {
                if (pingpong)
                {
                    if (!reversing && current + 1 >= sprites.Count) reversing = true;
                    else if (current == 0) reversing = false;
                    current += reversing ? -1 : 1;
                }
                else current++;
                current %= sprites.Count;
            }
            else
            {
                print(string.Format("Failed to set sprite to {0}. Disabling....", target.name));
                enabled = false;
            }
        }
    }
}
