﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class SimpleTransformAnimation : MonoBehaviour
{
    public bool local = true;
    public bool shouldReset = true;
    public bool disableOnDone = true;
    public bool separateTime;

    public bool doTrans;
    public bool doScale;
    public bool doRot;

    public float transTime;
    public float scaleTime;
    public float rotTime;

    public float transDelay;
    public float scaleDelay;
    public float rotDelay;

    public bool separatePos;
    public AnimationCurve deltaPosX = AnimationCurve.Constant(0, 1, 0);
    public AnimationCurve deltaPosY = AnimationCurve.Constant(0, 1, 0);
    public AnimationCurve deltaPosZ = AnimationCurve.Constant(0, 1, 0);

    public bool separateScale;
    public AnimationCurve deltaScaleX = AnimationCurve.Constant(0, 1, 1);
    public AnimationCurve deltaScaleY = AnimationCurve.Constant(0, 1, 1);
    public AnimationCurve deltaScaleZ = AnimationCurve.Constant(0, 1, 1);

    public bool separateRot;
    public AnimationCurve deltaRotX = AnimationCurve.Constant(0, 1, 0);
    public AnimationCurve deltaRotY = AnimationCurve.Constant(0, 1, 0);
    public AnimationCurve deltaRotZ = AnimationCurve.Constant(0, 1, 0);

    public Action callback;

    float passTransTime;
    float passScaleTime;
    float passRotTime;
    Vector3 oriPos;
    Vector3 oriScale;
    Quaternion oriRot;
    bool done;

    private void OnEnable()
    {
        passTransTime = -transDelay;
        passScaleTime = -scaleDelay;
        passRotTime = -rotDelay;
        oriPos = local ? transform.localPosition : transform.position;
        oriScale = local ? transform.localScale : transform.lossyScale;
        oriRot = local ? transform.localRotation : transform.rotation;
        done = false;
    }

    void Update()
    {
        if (done) return;

        if (doTrans && transTime > 0 && passTransTime <= transTime)
        {
            passTransTime += Time.deltaTime;
            if (passTransTime >= 0)
            {
                float curvePos = Mathf.Clamp01(passTransTime / transTime);
                Vector3 delta = new Vector3(deltaPosX.Evaluate(curvePos), deltaPosY.Evaluate(curvePos), deltaPosZ.Evaluate(curvePos));
                if (local) transform.localPosition = oriPos + delta;
                else transform.position = oriPos + delta;
            }
        }
        if (doScale && scaleTime > 0 && passScaleTime <= scaleTime)
        {
            passScaleTime += Time.deltaTime;
            if (passScaleTime >= 0)
            {
                float curvePos = Mathf.Clamp01(passScaleTime / scaleTime);
                float x = oriScale.x * deltaScaleX.Evaluate(curvePos);
                float y = oriScale.y * deltaScaleY.Evaluate(curvePos);
                float z = oriScale.z * deltaScaleZ.Evaluate(curvePos);
                Vector3 scale = new Vector3(x, y, z);
                if (local) transform.localScale = scale;
                else transform.SetGlobalScale(scale);
            }
        }
        if (doRot && rotTime > 0 && passRotTime <= rotTime)
        {
            passRotTime += Time.deltaTime;
            if (passRotTime >= 0)
            {
                float curvePos = Mathf.Clamp01(passRotTime / rotTime);
                Vector3 delta = new Vector3(deltaRotX.Evaluate(curvePos), deltaRotY.Evaluate(curvePos), deltaRotZ.Evaluate(curvePos));
                if (local) transform.localRotation = Quaternion.Euler(delta) * oriRot;
                else transform.rotation = Quaternion.Euler(delta) * oriRot;
            }
        }

        passTransTime += Time.deltaTime;
        if (passTransTime >= transTime && passScaleTime >= scaleTime && passRotTime >= rotTime)
        {
            done = true;
            if (callback != null) callback.Invoke();
            if (shouldReset) ResetTransform();
            if (disableOnDone) enabled = false;
        }
    }

    private void ResetTransform()
    {
        if (local)
        {
            if (doTrans) transform.localPosition = oriPos;
            if (doScale) transform.localScale = oriScale;
            if (doRot) transform.localRotation = oriRot;
        }
        else
        {
            if (doTrans) transform.position = oriPos;
            if (doScale) transform.SetGlobalScale(oriScale);
            if (doRot) transform.rotation = oriRot;
        }
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(SimpleTransformAnimation))]
public class SimpleTransformAnimationEditor : Editor
{
    bool enable;
    bool time;
    bool trans;
    bool scale;
    bool rot;

    public override void OnInspectorGUI()
    {
        SimpleTransformAnimation t = target as SimpleTransformAnimation;

        t.local = EditorGUILayout.ToggleLeft("Local transformations", t.local);
        t.shouldReset = EditorGUILayout.ToggleLeft("Reset tranform on done", t.shouldReset);
        t.disableOnDone = EditorGUILayout.ToggleLeft("Disable on done", t.disableOnDone);

        if (enable = EditorGUILayout.Foldout(enable, "Transforms", true))
        {
            t.doTrans = EditorGUILayout.Toggle("Trans", t.doTrans);
            t.doScale = EditorGUILayout.Toggle("Scale", t.doScale);
            t.doRot = EditorGUILayout.Toggle("Rot", t.doRot);
        }

        if (time = EditorGUILayout.Foldout(time, "Time", true))
        {
            EditorGUI.indentLevel++;
            t.separateTime = EditorGUILayout.Toggle("Separate", t.separateTime);
            if (!t.separateTime)
            {
                t.transTime = t.scaleTime = t.rotTime = EditorGUILayout.FloatField("Time", t.transTime);
                t.transDelay = t.scaleDelay = t.rotDelay = EditorGUILayout.FloatField("Delay", t.transDelay);
            }
            else
            {
                t.transTime = EditorGUILayout.FloatField("Translate time", t.transTime);
                t.transDelay = EditorGUILayout.FloatField("Translate delay", t.transDelay);

                t.scaleTime = EditorGUILayout.FloatField("Scale time", t.scaleTime);
                t.scaleDelay = EditorGUILayout.FloatField("Scale delay", t.scaleDelay);

                t.rotTime = EditorGUILayout.FloatField("Rotation time", t.rotTime);
                t.rotDelay = EditorGUILayout.FloatField("Rotation delay", t.rotDelay);
            }
            EditorGUI.indentLevel--;
        }

        if (trans = EditorGUILayout.Foldout(trans, "Translate delta", true))
        {
            EditorGUI.indentLevel++;
            t.separatePos = EditorGUILayout.Toggle("Separate", t.separatePos);
            if (!t.separatePos)
            {
                AnimationCurve ac = EditorGUILayout.CurveField("Translate", t.deltaPosX);
                t.deltaPosX = new AnimationCurve(ac.keys);
                t.deltaPosY = new AnimationCurve(ac.keys);
                t.deltaPosZ = new AnimationCurve(ac.keys);
            }
            else
            {
                t.deltaPosX = EditorGUILayout.CurveField("X", t.deltaPosX);
                t.deltaPosY = EditorGUILayout.CurveField("Y", t.deltaPosY);
                t.deltaPosZ = EditorGUILayout.CurveField("Z", t.deltaPosZ);
            }
            EditorGUI.indentLevel--;
        }

        if (scale = EditorGUILayout.Foldout(scale, "Scale delta", true))
        {
            EditorGUI.indentLevel++;
            t.separateScale = EditorGUILayout.Toggle("Separate", t.separateScale);
            if (!t.separateScale)
            {
                AnimationCurve ac = EditorGUILayout.CurveField("Scale", t.deltaScaleX);
                t.deltaScaleX = new AnimationCurve(ac.keys);
                t.deltaScaleY = new AnimationCurve(ac.keys);
                t.deltaScaleZ = new AnimationCurve(ac.keys);
            }
            else
            {
                t.deltaScaleX = EditorGUILayout.CurveField("X", t.deltaScaleX);
                t.deltaScaleY = EditorGUILayout.CurveField("Y", t.deltaScaleY);
                t.deltaScaleZ = EditorGUILayout.CurveField("Z", t.deltaScaleZ);
            }
            EditorGUI.indentLevel--;
        }

        if (rot = EditorGUILayout.Foldout(rot, "Rotate delta", true))
        {
            EditorGUI.indentLevel++;
            t.separateRot = EditorGUILayout.Toggle("Separate", t.separateRot);
            if (!t.separateRot)
            {
                AnimationCurve ac = EditorGUILayout.CurveField("Rotation", t.deltaRotX);
                t.deltaRotX = new AnimationCurve(ac.keys);
                t.deltaRotY = new AnimationCurve(ac.keys);
                t.deltaRotZ = new AnimationCurve(ac.keys);
            }
            else
            {
                t.deltaRotX = EditorGUILayout.CurveField("X", t.deltaRotX);
                t.deltaRotY = EditorGUILayout.CurveField("Y", t.deltaRotY);
                t.deltaRotZ = EditorGUILayout.CurveField("Z", t.deltaRotZ);
            }
            EditorGUI.indentLevel--;
        }
    }
}
#endif