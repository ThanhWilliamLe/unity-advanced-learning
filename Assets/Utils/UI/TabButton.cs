﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TabButton : MonoBehaviour, IPointerClickHandler
{
    public TabController controller;
    public GameObject content;
    public Image graphic;

    public void OnPointerClick(PointerEventData eventData)
    {
        controller.TabButtonPressed(this);
    }
}
