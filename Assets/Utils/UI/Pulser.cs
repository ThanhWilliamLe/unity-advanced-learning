﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class Pulser : MonoBehaviour
{
    public float time = 0.5f;
    [Range(0, 1)]
    public float peak = 0.3f;
    public float scale = 1.2f;

    float timePassed;
    Vector3 oriScale;

    private void Start()
    {
        oriScale = transform.localScale;
    }

    private void OnEnable()
    {
        timePassed = 0;
    }

    private void Update()
    {
        float ratio = Mathf.Clamp01(timePassed / time);
        if (peak > 0 && ratio <= peak) ratio = ratio / peak;
        else ratio = (1 - ratio) / (1 - peak);

        transform.localScale = Vector3.Lerp(oriScale, oriScale * scale, ratio);

        timePassed += Time.deltaTime;
        if (timePassed > time)
        {
            enabled = false;
        }
    }
}