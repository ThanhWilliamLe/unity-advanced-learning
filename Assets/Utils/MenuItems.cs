﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.SceneManagement;
#endif

public static class MenuItems
{
#if UNITY_EDITOR

    #region Data

    [MenuItem("WillMenu/Clear Player Prefs")]
    public static void ClearPlayerPrefs()
    {
        PlayerPrefs.DeleteAll();
        PlayerPrefs.Save();
    }

    #endregion


    #region Play

    [MenuItem("WillMenu/Play from splash")]
    public static void PlayFromSplash()
    {
        if (!EditorApplication.isPlaying)
        {
            if (EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo())
            {
                LoadScene("SplashScene");
                EditorApplication.isPlaying = true;
            }
        }
    }

    #endregion



    #region Color

    [MenuItem("WillMenu/Set all color setters")]
    public static void SetColorAll()
    {
        List<KeyValuePair<ColorSetter, string>> setters = ObjectUtils.GetObjectsOfType<ColorSetter>();
        string s1 = "Set color for: ";
        string s2 = "Failed: ";
        int succeeded = 0;
        int failed = 0;
        foreach (KeyValuePair<ColorSetter, string> kvp in setters)
        {
            if (kvp.Key.SetColor())
            {
                if (succeeded > 0) s1 += ", ";
                s1 += string.Format("{0} ({1})", kvp.Key.name, kvp.Value);
                succeeded++;
            }
            else
            {
                if (failed > 0) s1 += ", ";
                s1 += string.Format("{0} ({1})", kvp.Key.name, kvp.Value);
                failed++;
            }
        }
        if (succeeded == 0) s1 += "none";
        Print(s1);
        if (failed > 0) Print(s2 + "(" + failed + ")");
    }

    #endregion



    #region Text

    [MenuItem("WillMenu/Text/Set selected as default font")]
    public static void SetDefaultFont()
    {
        Font f = Selection.activeObject as Font;
        if (f == null) return;

        Text[] texts = Object.FindObjectsOfType<Text>();
        foreach (Text t in texts)
        {
            if (t.font == null || t.font == Resources.GetBuiltinResource<Font>("Arial.ttf")) t.font = f;
        }
    }

    [MenuItem("WillMenu/Text/Set selected as default font", true)]
    [MenuItem("WillMenu/Text/Remove selected font from all", true)]
    public static bool FontValidation()
    {
        return Selection.activeObject is Font;
    }

    [MenuItem("WillMenu/Text/Remove selected font from all")]
    public static void RemoveFont()
    {
        Font f = Selection.activeObject as Font;
        if (f == null) return;

        Text[] texts = Object.FindObjectsOfType<Text>();
        foreach (Text t in texts)
        {
            if (t.font != null && t.font == f) t.font = null;
        }
    }

    #endregion

    [MenuItem("WillMenu/Take screenshot _F12")]
    public static void ScreenShot()
    {
        string path = string.Format("{0}/Editor/Screenshots/Screen_{1}.png",
            Application.dataPath, System.DateTime.Now.ToString("yyMMdd_HHmmss"));
        ScreenCapture.CaptureScreenshot(path);
        Debug.Log("Screenshot saved at " + path);
    }

    public static void LoadScene(string s)
    {
        EditorSceneManager.OpenScene("Assets/Scenes/" + s + ".unity");
    }

    public static void Print(object s, params object[] ss)
    {
        if (s == null) Debug.Log("null");
        else Debug.LogFormat(s.ToString(), ss);
    }


#endif

}
